//Định nghĩa biến toàn cục
var gCustomerObject = {
    id: 0,
    firstName: "",
    lastName: "",
    address: "",
    phoneNumber: "",
    email: "",
    city: "",
    country: "",
    postalCode: "",
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gId = 0;
var gCustomerDb = new Array;

var gStt = 1;
function getSoThuTu() {
    return gStt++;
}
// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "firstName" },
        { "data": "lastName" },
        { "data": "address" },
        { "data": "phoneNumber" },
        { "data": "email" },
        { "data": "city" },
        { "data": "country" },
        { "data": "postalCode" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "3%"
        },

        {
            "targets": 9,
            "defaultContent": "<button class='btn btn-outline-success btn-sm edit-bt mr-1' title='edit button'><i class ='fas  fa-edit '></i> Edit</button>" +
                "<button class='btn btn-outline-danger btn-sm delete-bt mr-1' title='delete button'><i class ='fas fa-trash-alt '></i> Delete</button>" ,
                
                
            "width": "22%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllCustomersData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/customer/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gCustomerDb = responseObject;
            loadDataCustomerHandle(gCustomerDb)
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

getAllCustomersData();

//Load dữ liệu ra bảng
function loadDataCustomerHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//thêm mới
$("#add-new").on("click", addNewCustomer)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});

//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editCustomerData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteCustomerData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getOrdersData(this)
});


function getOrdersData(paramDetailButton) {
    var vRowIndex = $(paramDetailButton).parents("tr");
    var vCurrentCustomer = gDataTable.row(vRowIndex).data();
    const detailFormURL = "../admin/orders.html";
    urlSiteToOpen = detailFormURL + "?" + "id=" + vCurrentCustomer.id + "&firstName=" + vCurrentCustomer.firstName + "&lastName=" + vCurrentCustomer.lastName;
    window.location.href = urlSiteToOpen;
}

//Delete voucher
function deleteCustomerData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentCustomer = gDataTable.row(vRowIndex).data();
    gId = vCurrentCustomer.id;
    console.log("id: " + gId);
    deleteCustomerToForm(gId);
}

function deleteCustomerToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/customer/delete/" + paramId,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllCustomersData();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function editCustomerData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentCustomer = gDataTable.row(vRowIndex).data();
    gId = vCurrentCustomer.id;
    console.log()
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataCustomerToForm(gArrayIndex);
}
//Hàm xử lý nút thêm mới
function addNewCustomer() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataCustomerToForm(paramIndex) {

    $("#firstName-modal").val(gCustomerDb[paramIndex].firstName);
    $("#lastName-modal").val(gCustomerDb[paramIndex].lastName);
    $("#address-modal").val(gCustomerDb[paramIndex].address);
    $("#phoneNumber-modal").val(gCustomerDb[paramIndex].phoneNumber);
    $("#postalCode-modal").val(gCustomerDb[paramIndex].postalCode);
    $("#email-modal").val(gCustomerDb[paramIndex].state);
    $("#city-modal").val(gCustomerDb[paramIndex].city);
    $("#country-modal").val(gCustomerDb[paramIndex].country);

}
$("#send-data").on("click", saveCustomerData);
//Hàm save dữ liệu khi sửa
function saveCustomerData() {
    getCustomerData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveCustomer();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getCustomerData() {
    var vCustomerObject = {
        id: 0,
        firstName: "",
        lastName: "",
        address: "",
        phoneNumber: "",
        postalCode: "",
        email: "",
        city: "",
        country: "",
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vCustomerObject.id = gId;
    }
    vCustomerObject.firstName = $("#firstName-modal").val().trim();
    vCustomerObject.lastName = $("#lastName-modal").val().trim();
    vCustomerObject.address = $("#address-modal").val().trim();
    vCustomerObject.phoneNumber = $("#phoneNumber-modal").val().trim();
    vCustomerObject.postalCode = $("#postalCode-modal").val().trim();
    vCustomerObject.email = $("#email-modal").val().trim();
    vCustomerObject.city = $("#city-modal").val().trim();
    vCustomerObject.country = $("#country-modal").val().trim();
    gCustomerObject = vCustomerObject;
}

//Validate dữ liệu
function validateData() {
    if (gCustomerObject.firstName === "") {
        alert("Bạn chưa thêm firstName ");
        return false;
    }
    if (gCustomerObject.lastName === "") {
        alert("Bạn chưa thêm lastName ");
        return false;
    }
    if (gCustomerObject.address === "") {
        alert("Bạn chưa thêm address ");
        return false;
    }
    if (gCustomerObject.phoneNumber === "") {
        alert("Bạn chưa thêm phoneNumber ");
        return false;
    }
    if (gCustomerObject.postalCode === "") {
        alert("Bạn chưa thêm postalCode ");
        return false;
    }
    if (gCustomerObject.email === "") {
        alert("Bạn chưa thêm email ");
        return false;
    }
    if (gCustomerObject.city === "") {
        alert("Bạn chưa thêm city ");
        return false;
    }
    if (gCustomerObject.country === "") {
        alert("Bạn chưa thêm country ");
        return false;
    }
    return true;
}

//Hàm save data
function saveCustomer() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataCustomer(gCustomerObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertCustomerData(gCustomerObject)
    }

}
//Hàm cập nhật
function updateDataCustomer(paramCustomer) {
    var vId = paramCustomer.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/customer/update/" + vId,
        type: "PUT",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramCustomer),
        dataType: "json",
        success: function (paramRes) {
            getAllCustomersData()
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm thêm mới
function insertCustomerData(paramCustomer) {
    $.ajax({
        url: "http://localhost:8080/api/auth/customer/create",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramCustomer),
        dataType: "json",
        success: function (paramRes) {
            getAllCustomersData()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    })
}

//Reset form
function resetForm() {
    $("#firstName-modal").val("");
    $("#lastName-modal").val("");
    $("#address-modal").val("");
    $("#phoneNumber-modal").val("");
    $("#postalCode-modal").val("");
    $("#email-modal").val("");
    $("#city-modal").val("");
    $("#country-modal").val("");
}