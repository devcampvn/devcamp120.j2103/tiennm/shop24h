
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
//Định nghĩa biến toàn cục
var gProductLineObject = {
    id: 0,
    productLine: "",
    imageProductLine: "",
    description: ""
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdProductLine = 0;
var gProductLineDb = new Array;

var gStt = 1;
function getSoThuTu() {
    return gStt++;
}
// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};

/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "productLine" },
        { "data": "imageProductLine" },
        { "data": "description" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 1,

            "width": "10%"
        },
        {
            "targets": 2,

            render: function (data, type, full, meta) {
                return '<img src="../img/' + data + '" width="100px" />';
            },
            "width": "10%"
        },
        {
            "targets": 3,
            render: function (data, type, full, meta) {
                if (type === 'display') {
                    data = strtrunc(data, 100);
                }

                return data;
            },
            "width": "50%"
        },
        {
            "targets": 4,
            "defaultContent": "<button class='btn btn-outline-success edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> Edit</button><button class='btn btn-outline-danger delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> Delete</button>",
            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllProductLineData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productline/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProductLineDb = responseObject;
            loadDataProductLineHandle(gProductLineDb)
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

getAllProductLineData();

//Load dữ liệu ra bảng
function loadDataProductLineHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//thêm mới
$("#add-new").on("click", addNewProductLine)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editProductLineData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteProductLineData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getProductData(this)
});

function getProductData(paramDetailButton) {
    var vRowIndex = $(paramDetailButton).parents("tr");
    var vCurrentProductLine = gDataTable.row(vRowIndex).data();
    const detailFormURL = "../admin/list-product.html";
    urlSiteToOpen = detailFormURL + "?" + "id=" + vCurrentProductLine.id;
    window.location.href = urlSiteToOpen;
}

//Delete voucher
function deleteProductLineData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentProductLine = gDataTable.row(vRowIndex).data();
    gIdProductLine = vCurrentProductLine.id;
    console.log("id: " + gIdProductLine);
    deleteProductLineToForm(gIdProductLine);
}

function deleteProductLineToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/admin/productLine/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllProductLineData();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function editProductLineData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentProductLine = gDataTable.row(vRowIndex).data();
    gIdProductLine = vCurrentProductLine.id;
    console.log()
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataProductLineToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewProductLine() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataProductLineToForm(paramIndex) {

    $("#productLine-modal").val(gProductLineDb[paramIndex].productLine);
    $("#productLineImage-modal").val(gProductLineDb[paramIndex].imageProductLine);
    $("#desc-modal").val(gProductLineDb[paramIndex].description);

}
$("#send-data").on("click", saveProductLineData);
//Hàm save dữ liệu khi sửa
function saveProductLineData() {
    getProductLineData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveProductLine();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getProductLineData() {
    var vProductLineObject = {
        id: 0,
        productLine: "",
        imageProductLine: "",
        description: ""
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vProductLineObject.id = gIdProductLine;
    }
    vProductLineObject.productLine = $("#productLine-modal").val().trim();
    vProductLineObject.imageProductLine = $("#productLineImage-modal").val().trim();
    vProductLineObject.description = $("#desc-modal").val().trim();
    gProductLineObject = vProductLineObject;
}

//Validate dữ liệu
function validateData() {
    if (gProductLineObject.productLine === "") {
        alert("Bạn chưa thêm Product Line ");
        return false;
    }
    if (gProductLineObject.imageProductLine === "") {
        alert("Bạn chưa thêm Product Line Image");
        return false;
    }
    if (gProductLineObject.description === "") {
        alert("Bạn chưa thêm Mô tả ");
        return false;
    }
    return true;
}

//Hàm save data
function saveProductLine() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataProductLine(gProductLineObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertProductLineData(gProductLineObject)
    }

}
//Hàm cập nhật
function updateDataProductLine(paramProductLine) {
    var vId = paramProductLine.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productLine/update/" + vId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProductLine),
        dataType: "json",
        success: function (paramRes) {
            getAllProductLineData()
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm thêm mới
function insertProductLineData(paramProductLine) {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productLine/create",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProductLine),
        dataType: "json",
        success: function (paramRes) {
            getAllProductLineData()
        },
        error: function (ajaxContext) {
            alert("Bạn không có quyền thêm mới")
        }
    })
}

//Reset form
function resetForm() {
    $("#productLine-modal").val("");
    $("#productLineImage-modal").val("");
    $("#desc-modal").val("");
}