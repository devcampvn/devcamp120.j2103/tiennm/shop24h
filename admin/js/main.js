
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gCountDate = {
    orderDate: "",
    total: 0
}
var gOrderDate = new Array;
var gTotal = new Array;

var gTotalWeekObj = {
    orderWeek: "",
    total: 0
}

var gOrderWeek = new Array;
var gTotalWeek = new Array;
var gCustomer = new Array;
var gTotalPrice = new Array;
var gPrice = new Array;
var gCustomerType = {
    customer: "",
    total: 0
}
$('.select2').select2();
function getCountOrder() {
    $.ajax({
        url: "http://localhost:8080/api/auth/orders/count",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {

            gCountDate = responseObject;
            console.log(gCountDate);
            var vOrderDate = gCountDate.map(function (param) {
                return param.orderDate;
            });
            gOrderDate = vOrderDate;
            console.log(gOrderDate);
            var vTotal = gCountDate.map(function (param) {
                return param.total;
            });
            gTotal = vTotal;
            chartJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getCountOrder();
function chartJS() {
    var areaChartData = {
        labels: gOrderDate,
        datasets: [
            {
                label: 'Total price By date',
                backgroundColor: 'rgba(220,53,69,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: gTotal
            },
        ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0];
    barChartData.datasets[0] = temp0
    // barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })
}

function getTotalPriceByWeek() {
    $.ajax({
        url: "http://localhost:8080/api/auth/orders/countbyweek",
        type: "GET",
        dataType: 'json',
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (responseObject) {

            gTotalWeekObj = responseObject;
            console.log(gTotalWeekObj);
            var vOrderWeek = gTotalWeekObj.map(function (param) {
                return param.orderWeek;
            });
            gOrderWeek = vOrderWeek;
            console.log(gOrderDate);
            var vTotalWeek = gTotalWeekObj.map(function (param) {
                return param.total;
            });
            gTotalWeek = vTotalWeek;
            lineChartJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getTotalPriceByWeek();
function lineChartJS() {
    var areaChartData = {
        labels: gOrderWeek,
        datasets: [
            {
                label: 'Total price by week',
                backgroundColor: 'rgba(255,193,7,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: gTotalWeek
            },
        ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChartWeek').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0];
    barChartData.datasets[0] = temp0
    // barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })
}

/** Phân loại khách hàng theo tổng tiền*/
$("#select-customer").change(function () {
    var vValue = "";
    $("#select-customer option").filter(":selected").each(function () {
        vValue = $(this).val();
    })
    switch (vValue) {
        case "Vip":
            getCustomerVipData();
            $("#export-excel").prop("href", "http://localhost:8080/api/auth/export/customerVip/excel");
            break;

        case "Silver":
            getCustomerSilverData();
            $("#export-excel").prop("href", "http://localhost:8080/api/auth/export/customerSilver/excel");
            break;

        case "Gold":
            getCustomerGoldData();
            $("#export-excel").prop("href", "http://localhost:8080/api/auth/export/customerGold/excel");
            break;

        case "Diamond":
            getCustomerDiamondData();
            $("#export-excel").prop("href", "http://localhost:8080/api/auth/export/customerDiamond/excel");
            break;
        default:
            getCustomerVipData();
            $("#export-excel").prop("href", "http://localhost:8080/api/auth/export/customerVip/excel");
    }
})
getCustomerVipData();
/** Lấy dữ liệu customer vip */
function getCustomerVipData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/findCustomer/vip",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {

            gCustomerType = responseObject;
            console.log(gCustomerType);
            var vCustomer = gCustomerType.map(function (param) {
                return param.customer;
            });
            gCustomer = vCustomer;
            console.log(gCustomer);
            var vTotalPrice = gCustomerType.map(function (param) {
                return param.total;
            });
            gTotalPrice = vTotalPrice;
            console.log(gTotalPrice);
            chartCustomerJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

/** Lấy dữ liệu customer silver */
function getCustomerSilverData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/findCustomer/silver",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {

            gCustomerType = responseObject;
            console.log(gCustomerType);
            var vCustomer = gCustomerType.map(function (param) {
                return param.customer;
            });
            gCustomer = vCustomer;
            console.log(gCustomer);
            var vTotalPrice = gCustomerType.map(function (param) {
                return param.total;
            });
            gTotalPrice = vTotalPrice;
            console.log(gTotalPrice);
            chartCustomerJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

/** Lấy dữ liệu customer gold */
function getCustomerGoldData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/findCustomer/gold",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {

            gCustomerType = responseObject;
            console.log(gCustomerType);
            var vCustomer = gCustomerType.map(function (param) {
                return param.customer;
            });
            gCustomer = vCustomer;
            console.log(gCustomer);
            var vTotalPrice = gCustomerType.map(function (param) {
                return param.total;
            });
            gTotalPrice = vTotalPrice;
            console.log(gTotalPrice);
            chartCustomerJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

/** Lấy dữ liệu customer diamond */
function getCustomerDiamondData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/findCustomer/diamond",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {

            gCustomerType = responseObject;
            console.log(gCustomerType);
            var vCustomer = gCustomerType.map(function (param) {
                return param.customer;
            });
            gCustomer = vCustomer;
            console.log(gCustomer);
            var vTotalPrice = gCustomerType.map(function (param) {
                return param.total;
            });
            gTotalPrice = vTotalPrice;
            console.log(gTotalPrice);
            chartCustomerJS()
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

/** Hàm hiển thị dữ liệu ra biểu đồ */
function chartCustomerJS(){
    var areaChartData = {
        labels: gCustomer,
        datasets: [
            {
                label: 'Lọc khách hàng theo tổng số tiền',
                backgroundColor: 'rgba(40,167,69,0.9)',
                borderColor: 'rgba(60,141,188,0.8)',
                pointRadius: false,
                pointColor: '#3b8bba',
                pointStrokeColor: 'rgba(60,141,188,1)',
                pointHighlightFill: '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data: gTotalPrice
            },
        ]
    }
    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChartCustomer').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0];
    barChartData.datasets[0] = temp0
    // barChartData.datasets[1] = temp0

    var barChartOptions = {
        responsive: true,
        maintainAspectRatio: false,
        datasetFill: false
    }

    new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })
}