"use strict";
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gId = gNewUrl.searchParams.get("id");
var gFirstName = gNewUrl.searchParams.get("firstName");
var gLastName = gNewUrl.searchParams.get("lastName");
//Định nghĩa biến toàn cục
var gOrderObject = {
    id: 0,
    orderDate: "",
    requiredDate: "",
    shippedDate: "",
    comments: "",
    status: "",
    customerId: 0,
    fullName: ""
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdOrder = 0;
var gOrderDb = new Array;

$('.datepicker').datepicker({
    format: 'dd/mm/yyyy',
   
});
var gStt = 1;
function getSoThuTu() {
    return gStt++;
}
//Get full name
function getFullName() {
    var vFullName = gFirstName + " " + gLastName;
    return vFullName;
}

// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "id" },
        { "data": "fullName" },
        { "data": "orderDate" },
        { "data": "requiredDate" },
        { "data": "shippedDate" },
        { "data": "status" },
        { "data": "comments" },
        { "data": "action" }
    ],
    "responsive": true, "lengthChange": false, "autoWidth": false,
    "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 1,
            "width": "10%"
        },
        {
            "targets": 7,
            "defaultContent": "<button class='btn btn-outline-success edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i></button>" +
                "<button class='btn btn-outline-danger delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i></button>" +
                "<button class='btn btn-outline-info   more-bt mr-1 ' title='orderDetail button'><i class='fas fa-sitemap'></i></button>",

            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllOrderData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/orders/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gOrderDb = responseObject;
            loadDataOrderHandle(gOrderDb)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

getAllOrderData();

//thêm mới
$("#add-new").on("click", addNewOrder)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editOrderData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteOrderData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getOrderIdData(this)
});

function getOrderIdData(paramButton) {
    var vRowIndex = $(paramButton).parents("tr");
    var vCurrentOrder = gDataTable.row(vRowIndex).data();
    gIdOrder = vCurrentOrder.id;
    getOrderDetailData(gIdOrder);

}

function getOrderDetailData(paramId) {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/orderdetails/order/" + paramId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            loadDataOrderDetailHandle(responseObject)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

function loadDataOrderDetailHandle(paramRes) {
    for (var bI = 0; bI < paramRes.length; bI++) {
        $("#productName-modal").val(paramRes[bI].productName);
        $("#quantityOrder-modal").val(paramRes[bI].quantityOrder);
        $("#priceEach-modal").val(paramRes[bI].priceEach);
    }

    $("#modal-detail").modal("show");
}


//Delete voucher
function deleteOrderData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentOrder = gDataTable.row(vRowIndex).data();
    gIdOrder = vCurrentOrder.id;
    console.log("id: " + gIdOrder);
    deleteOrderToForm(gIdOrder);
}

function deleteOrderToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/admin/orders/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllOrderData();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function editOrderData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentOrder = gDataTable.row(vRowIndex).data();
    gIdOrder = vCurrentOrder.id;
    console.log(gIdOrder)
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataOrderToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewOrder() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataOrderToForm(paramIndex) {

    $("#orderDate-modal").val(gOrderDb[paramIndex].orderDate.split("-").reverse().join("-"));
    $("#requiredDate-modal").val(gOrderDb[paramIndex].requiredDate.split("-").reverse().join("-"));
    $("#shippedDate-modal").val(gOrderDb[paramIndex].shippedDate.split("-").reverse().join("-"));
    $("#status-modal").val(gOrderDb[paramIndex].status);
    $("#comments-modal").val(gOrderDb[paramIndex].comments);

}
$("#send-data").on("click", saveOrderData);
//Hàm save dữ liệu khi sửa
function saveOrderData() {
    getOrderData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveOrder();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");
        toastr.success('Thêm thành công');
        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getOrderData() {
    var vOrderObject = {
        id: 0,
        orderDate: "",
        requiredDate: "",
        shippedDate: "",
        comments: "",
        status: "",
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vOrderObject.id = gIdOrder;
    }
    vOrderObject.orderDate = $("#orderDate-modal").val().trim().split("-").reverse().join("-");
    vOrderObject.requiredDate = $("#requiredDate-modal").val().trim().split("-").reverse().join("-");
    vOrderObject.shippedDate = $("#shippedDate-modal").val().trim().split("-").reverse().join("-");
    vOrderObject.status = $("#status-modal").val().trim();
    vOrderObject.comments = $("#comments-modal").val().trim();
    gOrderObject = vOrderObject;
    console.log(vOrderObject);
}

//Validate dữ liệu
function validateData() {
    if (gOrderObject.orderDate === "") {
        alert("Bạn chưa thêm orderDate ");
        return false;
    }
    if (gOrderObject.requiredDate === "") {
        alert("Bạn chưa thêm requiredDate ");
        return false;
    }
    return true;
}

//Hàm save data
function saveOrder() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataOrder(gOrderObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertOrderData(gOrderObject)
    }

}
//Hàm cập nhật
function updateDataOrder(paramOrder) {
    // var vCustomerId = gOrderObject.customerId;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/orders/update/" + gIdOrder,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrder),
        dataType: "json",
        success: function (paramRes) {
            getAllOrderData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm thêm mới
function insertOrderData(paramOrder) {
    var vCustomerId = gOrderObject.customerId;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/orders/create/customer/" + gId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrder),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            getAllOrderData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Load dữ liệu ra bảng
function loadDataOrderHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw().buttons().container().appendTo('#table-data_wrapper .col-md-6:eq(0)');
}

//Reset form
function resetForm() {
    $("#orderDate-modal").val("");
    $("#requiredDate-modal").val("");
    $("#shippedDate-modal").val("");
    $("#status-modal").val("");
    $("#comments-modal").val("");
}