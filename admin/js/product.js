"use strict";
// Summernote
$('#summernote').summernote()
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gId = gNewUrl.searchParams.get("id");
//Định nghĩa biến toàn cục
var gProductObject = {
    id: 0,
    productCode: "",
    productName: "",
    productDescription: "",
    quantityInStock: 0,
    price: 0,
    productColorId: 0,
    productBrandId: 0,
    productLineId: 0
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdProduct = 0;
var gProductDb = new Array;

var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};

/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "productName" },
        { "data": "productCode" },
        { "data": "productDescription" },
        { "data": "quantityInStock" },
        { "data": "price" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },

        {
            "targets": 5,
            render: function (data, type, full, meta) {
                return data.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
            },
            "width": "5%"
        },

        {
            "targets": 6,
            "defaultContent": "<button class='btn btn-outline-success btn-sm edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> </button>" +
                "<button class='btn btn-outline-danger btn-sm delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> </button>" +
                "<button class='btn btn-outline-info btn-sm  more-bt mr-1 ' title='orderDetail button'><i class='fas fa-sitemap'></i></button>",
            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllProductsData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/product/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProductDb = responseObject;
            loadDataProductHandle(gProductDb)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}

getAllProductsData();

/**Hàm lấy dữ liệu product color **/
function getAllProductColor() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productColor/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            loadDataProductColorHandle(responseObject)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
getAllProductColor()
/** Hàm load dữ liệu ra fontend */
function loadDataProductColorHandle(paramColor) {
    var vStatusSelect = $("#productColor");
    $("<option/>", {
        text: "Tất cả",
        val: ""
    }).appendTo(vStatusSelect);
    for (var bI = 0; bI < paramColor.length; bI++) {
        $("<option/>", {
            val: paramColor[bI].id,
            text: paramColor[bI].colorName
        }).appendTo(vStatusSelect);
    }
}

/**Hàm lấy dữ liệu product brand **/
function getAllProductBrand() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productBrand/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            loadDataProductBrandHandle(responseObject)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
getAllProductBrand()
/** Hàm load dữ liệu ra fontend */
function loadDataProductBrandHandle(paramBrand) {
    var vStatusSelect = $("#productBrand");
    $("<option/>", {
        text: "Tất cả",
        val: ""
    }).appendTo(vStatusSelect);
    for (var bI = 0; bI < paramBrand.length; bI++) {
        $("<option/>", {
            val: paramBrand[bI].id,
            text: paramBrand[bI].brandName
        }).appendTo(vStatusSelect);
    }
}

/**Hàm lấy dữ liệu product line **/
function getAllProductLine() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productline/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            loadDataProductLineHandle(responseObject)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
getAllProductLine()
/** Hàm load dữ liệu ra fontend */
function loadDataProductLineHandle(paramLine) {
    var vStatusSelect = $("#productLine");
    $("<option/>", {
        text: "Tất cả",
        val: ""
    }).appendTo(vStatusSelect);
    for (var bI = 0; bI < paramLine.length; bI++) {
        $("<option/>", {
            val: paramLine[bI].id,
            text: paramLine[bI].productLine
        }).appendTo(vStatusSelect);
    }
}
//thêm mới
$("#add-new").on("click", addNewProduct)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editProductData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteProductData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getOrderDetailData(this)
});

function getOrderDetailData(paramDetailButton) {
    var vRowIndex = $(paramDetailButton).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    console.log(vCurrentProduct.id);
    const detailFormURL = "../admin/productImage.html";
    var urlSiteToOpen = detailFormURL + "?" + "productId=" + vCurrentProduct.id;
    window.location.href = urlSiteToOpen;
}


//Delete voucher
function deleteProductData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    gIdProduct = vCurrentProduct.id;
    console.log("id: " + gIdProduct);
    deleteOrderToForm(gIdProduct);
}

function deleteOrderToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/admin/product/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllProductsData();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        })
    }
}
//Hàm sử lý nút sửa
function editProductData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    gIdProduct = vCurrentProduct.id;
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataProductToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewProduct() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataProductToForm(paramIndex) {

    $("#productCode-modal").val(gProductDb[paramIndex].productCode);
    $("#productName-modal").val(gProductDb[paramIndex].productName);
    $("#summernote").summernote('code' , gProductDb[paramIndex].productDescription);
    console.log($("#summernote").summernote('code' , gProductDb[paramIndex].productDescription));
    $("#quantityInStock-modal").val(gProductDb[paramIndex].quantityInStock);
    $("#buyPrice-modal").val(gProductDb[paramIndex].price);
    $("#productColor").val(gProductDb[paramIndex].productColorId);
    $("#productBrand").val(gProductDb[paramIndex].productBrandId);
    $("#productLine").val(gProductDb[paramIndex].productLineId);

}
$("#send-data").on("click", saveProductData);
//Hàm save dữ liệu khi sửa
function saveProductData() {
    getProductData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveProduct();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getProductData() {
    var vProductObject = {
        id: 0,
        productCode: "",
        productName: "",
        productDescription: "",
        quantityInStock: 0,
        price: 0,
        productColorId: 0,
        productBrandId: 0
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vProductObject.id = gIdProduct;
    }
    vProductObject.productCode = $("#productCode-modal").val().trim();
    vProductObject.productName = $("#productName-modal").val().trim();
    vProductObject.productDescription = $("#summernote").val();
    console.log(vProductObject.productDescription);
    vProductObject.quantityInStock = $("#quantityInStock-modal").val().trim();
    vProductObject.price = $("#buyPrice-modal").val().trim();
    vProductObject.productColorId = $("#productColor").val().trim();
    vProductObject.productBrandId = $("#productBrand").val().trim();
    vProductObject.productLineId = $("#productLine").val().trim();
    gProductObject = vProductObject;
}

//Validate dữ liệu
function validateData() {
    if (gProductObject.productCode === "") {
        alert("Bạn chưa thêm productCode ");
        return false;
    }
    if (gProductObject.productName === "") {
        alert("Bạn chưa thêm productName ");
        return false;
    }
    if (gProductObject.quantityInStock === "") {
        alert("Bạn chưa thêm quantityInStock ");
        return false;
    }
    if (gProductObject.price === "") {
        alert("Bạn chưa thêm buyPrice ");
        return false;
    }
    if (gProductObject.productColorId === "") {
        alert("Bạn chưa thêm productColor ");
        return false;
    }
    if (gProductObject.productBrandId === "") {
        alert("Bạn chưa thêm productBrand ");
        return false;
    }
    if (gProductObject.productLineId === "") {
        alert("Bạn chưa thêm productLine ");
        return false;
    }
    return true;
}

//Hàm save data
function saveProduct() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataProduct(gProductObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertProductData(gProductObject)
    }

}
//Hàm cập nhật
function updateDataProduct(paramProduct) {
    var vProductBrandId = paramProduct.productBrandId;
    var vProductColorId = paramProduct.productColorId;
    var vProductLineId = paramProduct.productLineId;
    var vId = paramProduct.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/product/update/" + vId + "/" + vProductLineId + "/" + vProductBrandId + "/" + vProductColorId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProduct),
        dataType: "json",
        success: function (paramRes) {
            getAllProductsData();
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    })
}

//Hàm thêm mới
function insertProductData(paramProduct) {
    var vProductBrandId = paramProduct.productBrandId;
    var vProductColorId = paramProduct.productColorId;
    var vProductLineId = paramProduct.productLineId;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/product/create/" + vProductLineId + "/" + vProductBrandId + "/" + vProductColorId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProduct),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            getAllProductsData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Bạn không có quyền thêm mới");
            
        }
    })
}

//Load dữ liệu ra bảng
function loadDataProductHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//Reset form
function resetForm() {
    $("#productCode-modal").val("");
    $("#productName-modal").val("");
    $("#summernote").summernote('code' , "");
    $("#quantityInStock-modal").val("");
    $("#buyPrice-modal").val("");
    $("#productColor").val("");
    $("#productBrand").val("");
    $("#productLine").val("");
}