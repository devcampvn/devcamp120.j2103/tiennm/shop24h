"use strict";
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gId = gNewUrl.searchParams.get("productId");
console.log(gId);
//Định nghĩa biến toàn cục
var gProductObject = {
    id: 0,
    imageName: "",
    imageUrl: "",
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdProduct = 0;
var gProductDb = new Array;

var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "imageName" },
        { "data": "imageUrl" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 1,
            "width": "15%"
        },
        {
            "targets": 2,

            render: function (data, type, full, meta) {
                return '<img src="../img/' +data+ '" width="100px" />';
            },
            "width": "10%"
        },

        {
            "targets": 3,
            "defaultContent": "<button class='btn btn-outline-success edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> Edit</button>" +
                "<button class='btn btn-outline-danger delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> Delete</button>",
            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllProductsData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productimages/productId/" + gId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProductDb = responseObject;
            loadDataProductHandle(gProductDb)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

getAllProductsData();

//thêm mới
$("#add-new").on("click", addNewProduct)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editProductData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteProductData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getOrderDetailData(this)
});

function getOrderDetailData(paramDetailButton) {
    var vRowIndex = $(paramDetailButton).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    console.log(vCurrentProduct.id);
    const detailFormURL = "../admin/productImage.html";
    var urlSiteToOpen = detailFormURL + "?" + "productId=" + vCurrentProduct.id;
    window.location.href = urlSiteToOpen;
}


//Delete voucher
function deleteProductData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    gIdProduct = vCurrentProduct.id;
    console.log("id: " + gIdProduct);
    deleteOrderToForm(gIdProduct);
}

function deleteOrderToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/admin/productImage/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllProductsData();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function editProductData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    gIdProduct = vCurrentProduct.id;
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataProductToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewProduct() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataProductToForm(paramIndex) {

    $("#productUrl-modal").val(gProductDb[paramIndex].imageUrl);
    $("#productName-modal").val(gProductDb[paramIndex].imageName);

}
$("#send-data").on("click", saveProductData);
//Hàm save dữ liệu khi sửa
function saveProductData() {
    getProductData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveProduct();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getProductData() {
    var vProductObject = {
        id: 0,
        imageName: "",
        imageUrl: "",
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vProductObject.id = gIdProduct;
    }
    vProductObject.imageUrl = $("#productUrl-modal").val().trim();
    vProductObject.imageName = $("#productName-modal").val().trim();
    gProductObject = vProductObject;
}

//Validate dữ liệu
function validateData() {
    if (gProductObject.imageUrl === "") {
        alert("Bạn chưa thêm imageUrl ");
        return false;
    }
    if (gProductObject.imageName === "") {
        alert("Bạn chưa thêm imageName ");
        return false;
    }
    return true;
}

//Hàm save data
function saveProduct() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataProduct(gProductObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertProductData(gProductObject)
    }

}
//Hàm cập nhật
function updateDataProduct(paramProduct) {
    var vId = paramProduct.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productImages/update/" + vId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProduct),
        dataType: "json",
        success: function (paramRes) {
            getAllProductsData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm thêm mới
function insertProductData(paramProduct) {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/productImages/create/" + gId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramProduct),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            getAllProductsData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Load dữ liệu ra bảng
function loadDataProductHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//Reset form
function resetForm() {
    $("#productUrl-modal").val("");
    $("#productName-modal").val("");

}
