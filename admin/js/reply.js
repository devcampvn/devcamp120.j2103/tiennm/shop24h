"use strict";
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gId = gNewUrl.searchParams.get("reviewId");
console.log(gId);
//Định nghĩa biến toàn cục
var gReplyObject = {
    id: 0,
    reply: "",
    createDate: ""
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdReply = 0;
var gReplyDb = new Array;
var gUserId = 0;
var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};

/**Hàm lấy user Id */
function getIdUser(){
    $.ajax({
        url: "http://localhost:8080/api/auth/users/admin/me",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (pRes) {
            gUserId = pRes.id;
            console.log(gUserId)
            
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            
        }
    });
}
getIdUser();

/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "reply" },
        { "data": "createDate" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 3,
            "defaultContent": "<button class='btn btn-outline-success edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> Edit</button>" +
                "<button class='btn btn-outline-danger delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> Delete</button>",
            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllRepliesData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/reply/" + gId,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            gReplyDb = [];
            gReplyDb.push(responseObject)  ;
            console.log(gReplyDb);
            loadDataReplyHandle(gReplyDb)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(thrownError)
        }
    });
}

getAllRepliesData();

//thêm mới
$("#add-new").on("click", addNewReply)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editReplyData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteReplyData(this)
});


//Delete voucher
function deleteReplyData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentReply = gDataTable.row(vRowIndex).data();
    gIdReply = vCurrentReply.id;
    console.log("id: " + gIdReply);
    deleteReplyToForm(gIdReply);
}

function deleteReplyToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/reply/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                gReplyDb = [];
                loadDataReplyHandle(gReplyDb)
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function editReplyData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentReply = gDataTable.row(vRowIndex).data();
    gIdReply = vCurrentReply.id;
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataProductToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewReply() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataProductToForm(paramIndex) {

    $("#reply-modal").val(gReplyDb[paramIndex].reply);
    $("#createDate-modal").val(gReplyDb[paramIndex].createDate.split("-").reverse().join("-"));

}
$("#send-data").on("click", saveReplyData);
//Hàm save dữ liệu khi sửa
function saveReplyData() {
    getReplyData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveReply();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getReplyData() {
    var vReplyObject = {
        id: 0,
        reply: "",
        createDate: ""
    };
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        vReplyObject.id = gIdReply;
    }
    vReplyObject.reply = $("#reply-modal").val().trim();
    vReplyObject.createDate = $("#createDate-modal").val().split("-").reverse().join("-");
    gReplyObject = vReplyObject;
}

//Validate dữ liệu
function validateData() {
    if (gReplyObject.reply === "") {
        alert("Bạn chưa thêm reply ");
        return false;
    }
    if (gReplyObject.createDate === "") {
        alert("Bạn chưa thêm createDate ");
        return false;
    }
    return true;
}

//Hàm save data
function saveReply() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataReply(gReplyObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertReplyData(gReplyObject)
    }

}
//Hàm cập nhật
function updateDataReply(paramReply) {
    var vId = paramReply.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/reply/update/" + vId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramReply),
        dataType: "json",
        success: function (paramRes) {
            getAllRepliesData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm thêm mới
function insertReplyData(paramReply) {
    $.ajax({
        url: "http://localhost:8080/api/auth/reply/create/" + gId + "/" + gUserId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramReply),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            getAllRepliesData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log("bạn không thể thêm mới")
        }
    })
}

//Load dữ liệu ra bảng
function loadDataReplyHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//Reset form
function resetForm() {
    $("#reply-modal").val("");
    $("#createDate-modal").val("");

}
