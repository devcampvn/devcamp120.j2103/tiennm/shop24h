"use strict";
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
//Định nghĩa biến toàn cục
var gReviewObject = {
    id: 0,
    userName: "",
    review: "",
    rating: 0,
    productName: "",
    createDate: ""
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdReview = 0;
var gReviewDb = new Array;
// Summernote
$('#summernote').summernote()
var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

function getStar(paramData){
    var vHtml = "";
        if(paramData === 1){
           
            vHtml = `<div class="mb-3">
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
            </div>`;
            
        }
        else if(paramData === 2){
            vHtml = `<div class="mb-3">
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
            </div>`;
            
        }
        else if(paramData === 3){
            vHtml = `<div class="mb-3">
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
            </div>`;
            
        }
        else if(paramData === 4){
            vHtml = `<div class="mb-3">
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-muted mr-1 main_star"></i>
            </div>`;
            
        }
        else if(paramData === 5){
            vHtml = `<div class="mb-3">
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
                <i class="fas fa-star text-warning mr-1 main_star"></i>
            </div>`;
            
        }
        return vHtml;
    
}
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "userName" },
        { "data": "review" },
        { "data": "rating" },
        { "data": "productName" },
        { "data": "createDate" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 3,
            render: function (data, type, full, meta) {
                return getStar(data);
            },
            "width": "18%"
        },

        {
            "targets": 6,
            "defaultContent": "<button class='btn btn-outline-success edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> Xem chi tiết</button>" +
                              "<button class='btn btn-outline-danger delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> Delete</button>" +
                              "<button class='btn btn-outline-info   more-bt mr-1 ' title='orderDetail button'><i class='fas fa-sitemap'></i>Trả lời</button>",
            "width": "30%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllReviewData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/admin/review/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gReviewDb = responseObject;
            loadDataReviewHandle(gReviewDb)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    });
}

getAllReviewData();

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    reviewCommentData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteReviewData(this)
});

//Liên kết sang trang product
$("#table-data").on("click", ".more-bt", function () {
    getReplyData(this)
});

function getReplyData(paramDetailButton) {
    var vRowIndex = $(paramDetailButton).parents("tr");
    var vCurrentReview = gDataTable.row(vRowIndex).data();
    console.log(vCurrentReview.id);
    const detailFormURL = "../admin/reply.html";
    var urlSiteToOpen = detailFormURL + "?" + "reviewId=" + vCurrentReview.id;
    window.location.href = urlSiteToOpen;
}


//Delete voucher
function deleteReviewData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentReview = gDataTable.row(vRowIndex).data();
    gIdReview = vCurrentReview.id;
    console.log("id: " + gIdReview);
    deleteReviewToForm(gIdReview);
}

function deleteReviewToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/admin/review/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllReviewData();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        })
    }
}
//Hàm sử lý nút sửa
function reviewCommentData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentReview = gDataTable.row(vRowIndex).data();
    gIdReview = vCurrentReview.id;
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataProductToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewProduct() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataProductToForm(paramIndex) {

    $("#userName-modal").val(gReviewDb[paramIndex].userName);
    $("#review-modal").val(gReviewDb[paramIndex].review);
    if(gReviewDb[paramIndex].rating === 1){
        var vStar = `
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        `
        $("#rating-star").append(vStar)
    }
    else if(gReviewDb[paramIndex].rating === 2){
        var vStar = `
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        `
        $("#rating-star").append(vStar)
    }
    else if(gReviewDb[paramIndex].rating === 3){
        var vStar = `
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        `
        $("#rating-star").append(vStar)
    }
    else if(gReviewDb[paramIndex].rating === 4){
        var vStar = `
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-muted mr-1 main_star"></i>
        `
        $("#rating-star").append(vStar)
    }
    else if(gReviewDb[paramIndex].rating === 5){
        var vStar = `
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        <i class="fas fa-star text-warning mr-1 main_star"></i>
        `
        $("#rating-star").append(vStar)
    }
    $("#productName-modal").val(gReviewDb[paramIndex].productName);
    $("#dateCreate-modal").val(gReviewDb[paramIndex].createDate.split("-").reverse().join("-"));
   
}

//Load dữ liệu ra bảng
function loadDataReviewHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//Reset form
function resetForm() {
    $("#userName-modal").val("");
    $("#review-modal").val("");
    $("#productName-modal").val("");
    $("#dateCreate-modal").val("");
    $("#rating-star").html("")
}
