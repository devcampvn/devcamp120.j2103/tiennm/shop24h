"use strict";
//Định nghĩa biến toàn cục
var gUserObject = {
    id: 0,
    username: "",
    email: "",
    password: "",
    roles: [],
    role: []
};
var gFORM_STATUS_INSERT = "INSERT";
var gFORM_STATUS_UPDATE = "UPDATE";
var gFormStatus = gFORM_STATUS_INSERT;
var gArrayIndex = -1;
var gIdUser = 0;
var gToken = getCookie("token");
if (gToken == "") {
    window.location.href = "../signin.html";
}
var gUserDb = new Array;
var gRole = new Object();
var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

// Truncate a string
function strtrunc(str, max, add) {
    add = add || '...';
    return (typeof str === 'string' && str.length > max ? str.substring(0, max) + add : str);
};

/** Hàm get Role **/
function getRoles(paramRole) {
    var vRoleName = "";
    for (var bI = 0; bI < paramRole.length; bI++) {
        if (paramRole.length == 0) {
            vRoleName = paramRole[bI].name;
        } else if (paramRole.length > 0) {
            vRoleName += paramRole[bI].name;
        }

    }
    return vRoleName;
}
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "STT" },
        { "data": "username" },
        { "data": "email" },
        { "data": "password" },
        { "data": "roles" },
        { "data": "action" }
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },
        {
            "targets": 4,
            render: function (data, type, full, meta) {
                return getRoles(data);
            },
            "width": "5%"
        },
        {
            "targets": 5,
            "defaultContent": "<button class='btn btn-outline-success btn-sm edit-bt mr-2' title='edit button'><i class ='fas  fa-edit '></i> </button>" +
                "<button class='btn btn-outline-danger btn-sm delete-bt mr-2' title='delete button'><i class ='fas fa-trash-alt '></i> </button>",

            "width": "15%"
        }

    ]
})

//Hàm lấy dữ liệu voucher qua API
function getAllUserData() {
    $.ajax({
        url: "http://localhost:8080/api/auth/users/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            gUserDb = responseObject;
            console.log(gUserDb);
            loadDataUserHandle(gUserDb)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}

getAllUserData();

/**Hàm lấy dữ liệu product color **/
function getAllRole() {
    $.ajax({
        url: "http://localhost:8080/api/auth/roles/all",
        type: "GET",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        dataType: 'json',
        success: function (responseObject) {
            
            gRole = responseObject;
            console.log(gRole);
            loadDataRoleHandle(gRole)
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
getAllRole()
/** Hàm load dữ liệu ra fontend */
function loadDataRoleHandle(paramRole) {
    var vRoleSelect = $("#role_select");
    $("<option/>", {
        text: "Tất cả",
        val: ""
    }).appendTo(vRoleSelect);
    for (var bI = 0; bI < paramRole.length; bI++) {
        $("<option/>", {
            val: paramRole[bI].id,
            text: paramRole[bI].name
        }).appendTo(vRoleSelect);
    }
}


//thêm mới
$("#add-new").on("click", addNewUser)

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


//Sự kiện nút sửa
$("#table-data").on("click", ".edit-bt", function () {
    editUserData(this)
})
//Delete user
$("#table-data").on("click", ".delete-bt", function () {
    deleteUserData(this)
});




//Delete voucher
function deleteUserData(paramDelete) {
    var vRowIndex = $(paramDelete).parents("tr");
    var vCurrentUser = gDataTable.row(vRowIndex).data();
    gIdUser = vCurrentUser.id;
    console.log("id: " + gIdUser);
    deleteOrderToForm(gIdUser);
}

function deleteOrderToForm(paramId) {
    var vConfirm = confirm("Are you sure delete item?");
    if (vConfirm) {
        $.ajax({
            url: "http://localhost:8080/api/auth/users/delete/" + paramId,
            type: "DELETE",
            headers: {
                "Authorization": "Bearer " + gToken
            },
            dataType: "json",
            contentType: "application/json;charset=UTF-8",
            success: function (paramRes) {
                getAllUserData();
            },
            error: function (ajaxContext) {
                alert(ajaxContext.responseText)
            }
        })
    }
}
//Hàm sử lý nút sửa
function editUserData(paramEditButton) {
    gFormStatus = gFORM_STATUS_UPDATE;
    $("#modal-info").modal("show");
    var vRowIndex = $(paramEditButton).parents("tr");
    var vCurrentUser = gDataTable.row(vRowIndex).data();
    gIdUser = vCurrentUser.id;
    gArrayIndex = gDataTable.row(vRowIndex).index();
    loadDataUserToForm(gArrayIndex);
}

//Hàm xử lý nút thêm mới
function addNewUser() {
    $("#modal-info").modal("show");
}

//Hàm load dữ liệu vào form
function loadDataUserToForm(paramIndex) {

    $("#userName-modal").val(gUserDb[paramIndex].username);
    $("#email-modal").val(gUserDb[paramIndex].email);
    $("#password-modal").val(gUserDb[paramIndex].password);

    for (var bI = 0; bI < gUserDb[paramIndex].roles.length; bI++) {
        $("#role_select").val(gUserDb[paramIndex].roles[bI].id);
    }

}
$("#send-data").on("click", saveProductData);
//Hàm save dữ liệu khi sửa
function saveProductData() {
    getProductData();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveProduct();
        //B4:đóng modal
        resetForm();
        $("#modal-info").modal("hide");

        gFormStatus = gFORM_STATUS_INSERT;
        gArrayIndex = -1;

    }
}
//Lấy dữ liệu sửa
function getProductData() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        var vUserObject = {
            id: 0,
            username: "",
            email: "",
            password: "",
            roles: [],
        };
        var vRole = {
            id : 0 ,
            name: ""
        };
        vUserObject.id = gIdUser;
        vUserObject.username = $("#userName-modal").val().trim();
        vUserObject.email = $("#email-modal").val().trim();
        vUserObject.password = $("#password-modal").val().trim();
        vRole.id = $("#role_select").val().trim();
        for(var bI = 0 ; bI < gRole.length ; bI++){
            if(vRole.id == gRole[bI].id){
                vRole.name = gRole[bI].name;
                vUserObject.roles.push(vRole);
            }
        }
        gUserObject = vUserObject;
    } else if (gFormStatus === gFORM_STATUS_INSERT) {
        var vUserObject = {
            username: "",
            email: "",
            password: "",
            role: [],
        };
        vUserObject.username = $("#userName-modal").val().trim();
        vUserObject.email = $("#email-modal").val().trim();
        vUserObject.password = $("#password-modal").val().trim();
        var vRoleId = ("#role_select").val().trim();
        for(var bI = 0 ; bI < gRole.length ; bI++){
            if(vRoleId == gRole[bI].id){
                vUserObject.role.push(gRole[bI].name);
            }
        }
        gUserObject = vUserObject;
    }


}


//Validate dữ liệu
function validateData() {
    if (gUserObject.userName === "") {
        alert("Bạn chưa thêm userName ");
        return false;
    }
    if (gUserObject.email === "") {
        alert("Bạn chưa thêm email ");
        return false;
    }
    if (gUserObject.password === "") {
        alert("Bạn chưa thêm password ");
        return false;
    }
    if (gUserObject.roleName === "") {
        alert("Bạn chưa thêm roleName ");
        return false;
    }

    return true;
}

//Hàm save data
function saveProduct() {
    if (gFormStatus === gFORM_STATUS_UPDATE) {
        updateDataUser(gUserObject);
    }
    else if (gFormStatus === gFORM_STATUS_INSERT) {
        insertUserData(gUserObject)
    }

}
//Hàm cập nhật
function updateDataUser(paramUser) {
    var vUserId = paramUser.id;
    $.ajax({
        url: "http://localhost:8080/api/auth/users/update/" + vUserId,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramUser),
        dataType: "json",
        success: function (paramRes) {
            getAllUserData();
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    })
}

//Hàm thêm mới
function insertUserData(paramUser) {
    $.ajax({
        url: "http://localhost:8080/api/auth/users/create",
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramUser),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            getAllUserData();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Load dữ liệu ra bảng
function loadDataUserHandle(paramResponseObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramResponseObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}

//Reset form
function resetForm() {
    $("#userName-modal").val("");
    $("#email-modal").val("");
    $("#password-modal").val("");
    $("#role_select").val("");
}