/** Tạo biến cục bộ */
var gListItemCart = new Object();
var gTotal = 0;
var gSub_Total = 0;
var gOrderValue = 0;
gKeyLocalStorangeOrder = "orderDetail";
/** Lấy dữ liệu từ localStorange */
function getDataToLocalStorange() {
    var vJsonListCart = localStorage.getItem("listItemCart");
    if (vJsonListCart == []) {
        $(".order-detail").hide();
        $(".total-product").hide();
    } else {
        gListItemCart = JSON.parse(vJsonListCart);
        console.log(gListItemCart)
        getDataProductTable(gListItemCart)
    }
}
getDataToLocalStorange()
/** Hiển thị data ra table */
function getDataProductTable(paramListCart) {
    var vItemList = $("#content-item");
    vItemList.children().remove();
    gTotal = 0;
    for (var bI = 0; bI < paramListCart.length; bI++) {
        var vSub_Total = parseInt(paramListCart[bI].product_Price) * parseInt(paramListCart[bI].quantity);
        gTotal += vSub_Total;
        var vItemCart = `
            <tr>
                <td><div id="removeItem" data-id=${paramListCart[bI].productId}><i class="fas fa-times-circle"></i></div></td>
                <td><img src="./img/${paramListCart[bI].product_Image}" width="100px"
                        alt=""></td>
                <td class="pName">${paramListCart[bI].product_Name}</td>
                <td>
                    <div class="qty-and-message">
                        <div class="quantity-input-wrapper">
                            <div class="group-input">
                                <div class="minus" style="pointer-events: all!important;">
                                    <i style="background-color: rgb(40, 138, 214);" id="custom"></i>
                                </div>
                                <div class="number-product">${paramListCart[bI].quantity}</div>
                                <div class="plus" style="pointer-events: all;">
                                    <i style="background-color: rgb(40, 138, 214);"></i>
                                    <i style="background-color: rgb(40, 138, 214);"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td class="price-col">${paramListCart[bI].product_Price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
                <td>${vSub_Total.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
            </tr>
        `
        vItemList.append(vItemCart);
    }
    var vTotalTable = $("#total");
    vTotalTable.html("");
    var vCartTotal = `
            <tr>
                <td>Tạm tính</td>
                <td class="font-weight-bold">${gTotal.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
            </tr>
            <tr>
                <td>Tổng tiền</td>
                <td class="font-weight-bold text-danger">${gTotal.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
            </tr>
        `
    vTotalTable.append(vCartTotal);

}

//Tạo hành động xoá sản phẩm trong giỏ hàng
$("#content-item").on("click", "#removeItem", function () {
    deleteItemToCart(this)
});

/** Hàm thực thi câu lệnh xoá  */
function deleteItemToCart(paramBtn) {

    var vDataButon = $(paramBtn).data();
    var vId = vDataButon.id;
    console.log(vId);
    for (var bI = 0; bI < gListItemCart.length; bI++) {
        if (gListItemCart[bI].productId == vId) {
            gListItemCart.splice(bI, 1);
            $(paramBtn).closest("tr").remove();
        }
    }
    localStorage.setItem("listItemCart", JSON.stringify(gListItemCart));
    getDataToLocalStorange();
}

/** Hàm thực hiện trừ */
$("#content-item").on('click', ".minus", function () {
    var quantityEl = $(this).next();
    var value = parseInt(quantityEl.text()) - 1;
    value = value < 1 ? 1 : value;
    quantityEl.html(value);
    var vParrent = $(this).parents('tr');
    var vIndex = vParrent.index();
    console.log(vIndex);
    gListItemCart[vIndex].quantity = parseInt(value);
    localStorage.setItem("listItemCart", JSON.stringify(gListItemCart));
    getDataToLocalStorange();

    if (value == 1) {

        $("#custom").css("background-color", "rgb(204, 204, 204)");
    } else if (value > 1) {
        $("#custom").css("background-color", "rgb(40, 138, 214)")
    }
})

/** Hàm thực hiện cộng */
$("#content-item").on('click', ".plus", function () {
    var quantityEl = $(this).prev();
    var value = parseInt(quantityEl.text()) + 1;
    quantityEl.html(value);
    var vParrent = $(this).parents('tr');
    var vIndex = vParrent.index();
    console.log(vIndex);
    gListItemCart[vIndex].quantity = value;
    localStorage.setItem("listItemCart", JSON.stringify(gListItemCart));
    getDataToLocalStorange();

    $(".minus").css("pointer-events", "all");
    $("#custom").css("background-color", "rgb(40, 138, 214)");
})

/** Hàm thực hiện thanh toán */
$("#checkout").on("click" , checkOutOnClick)
function checkOutOnClick(){
    var shoppingCartURL = "checkout.html";
    urlSiteToOpen = shoppingCartURL;
    window.location.href = "./" + urlSiteToOpen;
}






