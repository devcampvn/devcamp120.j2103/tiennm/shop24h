// Khai báo các biến cục bộ
var gProduct = {
    id: 0,
    productCode: "",
    productName: "",
    imageUrl: "",
    productDescription: "",
    quantityInStock: "",
    price: "",
    brandId: 0,
    colorProductId: 0
}

var gproductLine = {
    id: 0,
    productLine: "",
    imageProductLine: "",
    description: "",
}

var gColorProduct = {
    id: 0,
    colorName: ""
}

var gBrandProduct = {
    id: 0,
    brandName: ""
}
var gPageNumberPresent = 0;
var gTotalPage = 0;
var gLimit = 6;
var gPageNumber = 0;
var gProductLineIdList = new Array;
var gProductBrandIdList = new Array;
var gProductColorIdList = new Array;

//Phân trang
function calculatingPagination(paramProduct) {
    // vNumberOfPage = Math.ceil(paramProduct / gLimit);
    vNumberOfPage = paramProduct.totalPages;
    gTotalPage = vNumberOfPage;
    $(".pagination").html("");
    var prevItem =
        `<li class="page-item-prev">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>`
    $(".pagination").append(prevItem);
    for (i = 0; i < vNumberOfPage; i++) {
        var pageItem =
            `<li class="page-item page-item-${i + 1}">
            <a class="page-link" href="#">${i + 1}</a>
        </li>`
        $(".pagination").append(pageItem);
    }
    $(".page-item-1 a").addClass("active");
    var nextItem =
        `<li class="page-item-next">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>`
    $(".pagination").append(nextItem);
    
}
getProductFromDB(gPageNumber, gLimit);
function getProductFromDB(paramPageNumber, paramLimit) {
    $.ajax({
        url: "http://localhost:8080/api/auth/products?start=" + paramPageNumber + "&end=" + paramLimit,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            // calculatingPagination(responseObject);
            gPageNumberPresent = responseObject.pageable.pageNumber;
            console.log("present: " + gPageNumberPresent);
            loadAllDataProduct(responseObject);
            calculatingPagination(responseObject)
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

function loadAllDataProduct(paramProduct) {
    var vProductList = $("#product-list .row");
    vProductList.html("");
    if(paramProduct.content.length  == 0){
        var vAlert = `<div class="alert alert-danger col-md-12" role="alert">
        Không có sản phẩm nào
      </div>`;
      vProductList.append(vAlert);
    }else{
        for (var bI = 0; bI < paramProduct.content.length; bI++) {
            var vItemProduct = `
                <div class="col-md-4 col-sm-6 product text-center">
                    <a href="./productdetail.html?id=${paramProduct.content[bI].id}">
                        <div class="anh">
                            <img src="img/${paramProduct.content[bI].productImages[0].imageUrl}" alt=""
                                class="img-fluid">
                        </div>
                        <h3 id="title">${paramProduct.content[bI].productName}</h3>
                    </a>
                    <p id="price">${paramProduct.content[bI].price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
                </div>
            `
            vProductList.append(vItemProduct);
            
        }
    }
    
    
}
$(".pagination").on("click", "li.page-item", loadDataAfterFirstPage);
$(".pagination").on("click", "li.page-item-prev", loadDataPreviousPage);
$(".pagination ").on("click", "li.page-item-next", loadDataNextPage);
function loadDataAfterFirstPage() {
    vPageNumber = parseInt($(this).find("a").text()) - 1;
    // console.log(vPageNumber);
    // localStorage.setItem("pagenumber", vPageNumber);
    // location.reload();
    getProductFromDB(vPageNumber, gLimit);
}

function loadDataPreviousPage() {
    var vPageNumber = gPageNumberPresent - 1;
    console.log(vPageNumber);
    if (vPageNumber > 0) {
        // localStorage.setItem("pagenumber", vPageNumber);
        // location.reload();
        getProductFromDB(vPageNumber, gLimit);
    } else {
        vPageNumber = 0;
        // localStorage.setItem("pagenumber", vPageNumber);
        // location.reload();
        getProductFromDB(vPageNumber, gLimit);
    }
}
function loadDataNextPage() {
    var vPageNumber = gPageNumberPresent + 1;
    if (vPageNumber < gTotalPage) {
        // localStorage.setItem("pagenumber", vPageNumber);
        // location.reload();
        getProductFromDB(vPageNumber, gLimit);
    } else {
        vPageNumber = gPageNumberPresent;
        // localStorage.setItem("pagenumber", vPageNumber);
        // location.reload();
        getProductFromDB(vPageNumber, gLimit);
    }

}
/** Hiển thị danh mục sản phẩm */
function getAllCatergory() {
    $.ajax({
        url: "http://localhost:8080/api/auth/productline/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gproductLine = responseObject;
            loadAllCatergoryHandle(gproductLine);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getAllCatergory()
//Hàm show catergory
function loadAllCatergoryHandle(paramProductLine) {
    var vCatergory = $("#filter-catergory");
    for (var bI = 0; bI < paramProductLine.length; bI++) {
        var vItemCatergory = '<div class="form-check">';
        vItemCatergory += '<input class="form-check-input" name="productLineId" type="checkbox" value="' + paramProductLine[bI].id + '" id="catergory-filter">';
        vItemCatergory += '<label class="form-check-label" for="catergory-filter">' + paramProductLine[bI].productLine + '</label>';
        vItemCatergory += '</div>';
        vCatergory.append(vItemCatergory);
    }

}

/** Hiển thị list color */
function getAllColor() {
    $.ajax({
        url: "http://localhost:8080/api/auth/productColor/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gColorProduct = responseObject;
            loadAllColorHandle(gColorProduct);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getAllColor()
//Hàm show color
function loadAllColorHandle(paramColor) {
    var vCatergory = $("#filter-color");
    for (var bI = 0; bI < paramColor.length; bI++) {
        var vItemCatergory = '<div class="form-check">';
        vItemCatergory += '<input class="form-check-input" type="checkbox" name="productColorId" value="' + paramColor[bI].id + '" id="color-filter">';
        vItemCatergory += '<label class="form-check-label" for="color-filter">' + paramColor[bI].colorName + '</label>';
        vItemCatergory += '</div>';
        vCatergory.append(vItemCatergory);
    }

}

/** Hiển thị list brand */
function getAllBrand() {
    $.ajax({
        url: "http://localhost:8080/api/auth/productBrand/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gColorProduct = responseObject;
            loadAllBrandHandle(gColorProduct);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getAllBrand()
//Hàm show brand
function loadAllBrandHandle(paramColor) {
    var vCatergory = $("#filter-brand");
    for (var bI = 0; bI < paramColor.length; bI++) {
        var vItemCatergory = '<div class="form-check">';
        vItemCatergory += '<input class="form-check-input" type="checkbox" name="productBrandId" value="' + paramColor[bI].id + '" id="brand-filter">';
        vItemCatergory += '<label class="form-check-label" for="brand-filter">' + paramColor[bI].brandName + '</label>';
        vItemCatergory += '</div>';
        vCatergory.append(vItemCatergory);
    }
}

/** Hàm tạo đối tượng product price */
function getProductPrice(){
    var vProduct = new Object();
    vProduct.minPrice = 0;
    vProduct.maxPrice = 0;
    return vProduct;
}
/**filter dữ liệu */
$( "#select-price" )
  .change(function() {
    var vPrice = getProductPrice();
    var values = "";
    $( "#select-price option" ).filter(":selected").each(function() {       
        values = $(this).val();
    });
    if(values == "filter-1"){
        vPrice.minPrice = 1000000;
        vPrice.maxPrice = 3000000;
        filterProductPrice(vPrice);
    }
    else if(values == "filter-2"){
        vPrice.minPrice = 3000000;
        vPrice.maxPrice = 6000000;
        filterProductPrice(vPrice);
        console.log(values);
    }
    else if(values == "filter-3"){
        vPrice.minPrice = 6000000;
        vPrice.maxPrice = 10000000;
        filterProductPrice(vPrice);
        console.log(values);
    }else{
        getProductFromDB(gPageNumber, gLimit);
    }
});
/** Load dữ liệu ra fontend */
function filterProductPrice(paramPrice){
    $.ajax({
        url: "http://localhost:8080/api/auth/productPriceList?start=" + gPageNumber + "&end=" + gLimit,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramPrice),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            gPageNumberPresent = paramRes.pageable.pageNumber;
            console.log("present: " + gPageNumberPresent);
            loadAllDataProduct(paramRes);
            calculatingPagination(paramRes)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}
var gReset = $("#reset").hide();
gReset.on("click" , resetAllFilter);
function resetAllFilter(){
    $("#filter-catergory #catergory-filter").prop( "checked", false );
    $("#filter-brand #brand-filter").prop( "checked", false );
    $("#filter-color #color-filter").prop( "checked", false );
    gReset.fadeIn().hide();
    getProductFromDB(gPageNumber, gLimit);
}
/** Get selected item catergory */
$( "#filter-catergory").on("change" ,"#catergory-filter" , function(){
    var vListCheckItems = $("#filter-catergory #catergory-filter");
    gProductLineIdList = [];
    vListCheckItems.filter(":checked").each(function(){
        gProductLineIdList.push(parseInt($(this).val()));
        gReset.fadeIn().show();
    })
   
  
   var vFilter = getAllFilterItem();
   console.log(vFilter.productLineIds);   
   getAllProductByFilter(vFilter);
   
})
/** Get selected item brand */
$( "#filter-brand").on("change" ,"#brand-filter" , function(){
    var vListCheckItems = $("#filter-brand #brand-filter");
    gProductBrandIdList = [];
    vListCheckItems.filter(":checked").each(function(){
        gProductBrandIdList.push(parseInt($(this).val()));
        gReset.fadeIn().show()
    })
    
    console.log(gProductBrandIdList);   
    var vFilter = getAllFilterItem();
    getAllProductByFilter(vFilter);
})
/** Get selected item color */
$( "#filter-color").on("change" ,"#color-filter" , function(){
    var vListCheckItems = $("#filter-color #color-filter");
    gProductColorIdList = [];
    vListCheckItems.filter(":checked").each(function(){
        gProductColorIdList.push(parseInt($(this).val()));
        gReset.fadeIn().show()
    })
    
    console.log(gProductColorIdList);
    var vFilter = getAllFilterItem();
    getAllProductByFilter(vFilter);
})

/** Lấy tất cả giá trị item đã select */
function getAllFilterItem(){
    var vProductFilter = new Object();
    vProductFilter.productLineIds = gProductLineIdList;
    vProductFilter.productBrandIds = gProductBrandIdList;
    vProductFilter.productColorIds = gProductColorIdList;
    return vProductFilter;
}

/** Hàm thực thi gửi request lên server */
function getAllProductByFilter(paramFilter){
    $.ajax({
        url: "http://localhost:8080/api/auth/filterProducts?start=" + gPageNumber + "&end=" + gLimit,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramFilter),
        dataType: "json",
        success: function (paramRes) {
            gPageNumberPresent = paramRes.pageable.pageNumber;
            console.log("present: " + gPageNumberPresent);
            loadAllDataProduct(paramRes);
            calculatingPagination(paramRes)
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

