/** Tạo biến cục bộ */
var gListItemCart = new Object();
var gCustomerData = new Object();
var gUserData = new Object();
var gTotal = 0;
var gSub_Total = 0
var gComment = $("#comment");
var gUsername = "";
var gEmail = "";
// console.log($("#comment").val());
const gCURRENT_PAGE = "./checkout.html";
localStorage.setItem('current_page', gCURRENT_PAGE);
const token = getCookie("token");
/** Lấy dữ liệu từ localStorange */
function getDataToLocalStorange() {
    var vJsonListCart = localStorage.getItem("listItemCart");
    if (vJsonListCart == [] || vJsonListCart == null) {
        $(".order-detail").hide();
        $(".total-product").hide();
    } else {
        gListItemCart = JSON.parse(vJsonListCart);
        console.log(gListItemCart)
        getDataProductTable(gListItemCart)
    }
}
getDataToLocalStorange()

/** Load dữ liệu ra bảng order */
function getDataProductTable(paramListCart) {
    var vItemList = $("#content-item");
    for (var bI = 0; bI < paramListCart.length; bI++) {
        var gSub_Total = parseInt(paramListCart[bI].product_Price) * parseInt(paramListCart[bI].quantity);
        gTotal += gSub_Total
        var vItemOrder = `
            <tr>
                <td> <span class="sp">${paramListCart[bI].product_Name}</span> × <span class="qty">${paramListCart[bI].quantity}</span></td>
                <td style="color:red; font-weight:bold">${gSub_Total.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
            </tr>
         `
        vItemList.append(vItemOrder);
    }
    var vSubTotal = `
        <tr>
            <td>Tạm tính</td>
            <td style="color:red; font-weight:bold">${gTotal.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
        </tr>
    `
    vItemList.append(vSubTotal);

    var vTotalPrice = `
        <tr>
            <td>Tổng tiền</td>
            <td style="color:red; font-weight:bold">${gTotal.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</td>
        </tr>
    `
    vItemList.append(vTotalPrice);
}

function checkDataUser() {
    if (token == "") {
        $("#myModal").modal("show");
        return;
    }
    $.ajax({
        url: "http://localhost:8080/api/auth/user/me",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + token
        },
        success: function (pRes) {

            gUsername = pRes.username;
            gEmail = pRes.email;
            console.log(gEmail);
            var vCustomerData = getDataCustomer();
            console.log("test : " + vCustomerData.email);
            /** Validte dữ liệu */
            if (validateData(vCustomerData.firstName, vCustomerData.lastName, vCustomerData.address, vCustomerData.phoneNumber, vCustomerData.email, vCustomerData.city, vCustomerData.country)) {

                /** Insert dữ liệu */
                insertCustomer(vCustomerData, gUsername);

            }

        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);

        }
    });
}
/** Truyền dữ liệu người dùng lên server */
$("#checkout").on("click", insertCustomerData)
function insertCustomerData() {
    /** Hàm kiểm tra tồn tại user */
    checkDataUser();
}



/** Hàm tạo dữ liệu người dùng */
function getDataCustomer() {
    var vDataCustomer = new Object();
    vDataCustomer.firstName = $("#fname").val().trim();
    vDataCustomer.lastName = $("#lname").val().trim();
    vDataCustomer.address = $("#address").val().trim();
    vDataCustomer.phoneNumber = $("#phoneNumber").val().trim();
    vDataCustomer.email = gEmail;
    vDataCustomer.city = $("#city").val().trim();
    vDataCustomer.country = $("#country").val().trim();
    vDataCustomer.postalCode = $("#postalCode").val().trim();
    return vDataCustomer;
}


/** Hàm tạo dữ liệu order */
function getOrderData() {
    var vOrder = new Object();
    vOrder.comments = gComment.val();
    vOrder.status = "open";
    return vOrder;
}

/** Hàm tạo dữ liệu order detail */
function getOrerDetailData() {
    var vOrderDetailObject = new Array;      
    for(var bI = 0 ; bI < gListItemCart.length ; bI++){  
        var vOrderDetails = {
            price_each : 0,
            quantity_order : 0
        };      
        vOrderDetails.quantity_order =gListItemCart[bI].quantity  ;
        vOrderDetails.price_each = gListItemCart[bI].quantity * gListItemCart[bI].product_Price;
        vOrderDetailObject.push(vOrderDetails);
    }
    
    
    return vOrderDetailObject;
}

/** Hàm insert Data */
function insertCustomer(paramCustomerData, paramUserName) {
    $.ajax({
        url: "http://localhost:8080/api/auth/customer/create/user?username=" + paramUserName,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        headers: {
            "Authorization": "Bearer " + token
        },
        data: JSON.stringify(paramCustomerData),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            var vId = paramRes.id;
            insertOrderData(vId);

            resetForm();
            var vAlerts = `
            <div class="alert alert-success" role="alert"> Đặt hàng thành công</div>
        `
            $("#alerts").append(vAlerts);
            
            localStorage.removeItem('listItemCart');
            
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    })
}

/** Hàm insert data order */
function insertOrderData(paramId) {
    var vOrderObject = getOrderData();
    $.ajax({
        url: "http://localhost:8080/api/auth/orders/create/" + paramId,
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        headers: {
            "Authorization": "Bearer " + token
        },
        data: JSON.stringify(vOrderObject),
        dataType: "json",
        success: function (paramRes) {
            console.log(paramRes);
            var vId = paramRes.id;
            insertOrderDetailData(vId);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    })
}
/** Hàm insert data order detail */
function insertOrderDetailData(paramId) {
    // var vOrderDetailObject = getOrerDetailData();

    for(var bI = 0 ; bI < gListItemCart.length ; bI++){  
        var vOrderDetails = {
            priceEach : 0,
            quantityOrder : 0
        };      
        vOrderDetails.quantityOrder =gListItemCart[bI].quantity  ;
        vOrderDetails.priceEach = gListItemCart[bI].quantity * gListItemCart[bI].product_Price;
        var vProductId =  gListItemCart[bI].productId;
        // vOrderDetailObject.push(vOrderDetails);
        $.ajax({
            url: "http://localhost:8080/api/auth/orderdetails/order/create/" + paramId + "/" + vProductId,
            type: "POST",
            contentType: "application/json;charset=UTF-8",
            headers: {
                "Authorization": "Bearer " + token
            },
            data: JSON.stringify(vOrderDetails),
            dataType: "json",
            success: function (paramRes) {
                console.log(paramRes);
                window.location.replace("./user-order.html");
    
            },
            error: function (xhr) {
                alert(xhr.status);
            }
        })
    }

    
}


/** Hàm reset  */
function resetForm() {
    $("#fname").val("");
    $("#lname").val("");
    $("#address").val("");
    $("#phoneNumber").val("");
    $("#city").val("");
    $("#country").val("");
    $("#postalCode").val("");
    $("#comment").val("");
    $("#orderDetail").hide();
}

/** Hàm kiểm tra điều kiện */
function validateData(paramFirstName, paramLastName, paramAddress, paramPhoneNumber, paramEmail, paramCity, paramCountry) {
    if (paramFirstName === "") {
        toastr.error("Bạn chưa thêm firstName ");
        return false;
    }
    if (paramLastName === "") {
        toastr.error("Bạn chưa thêm lastName ");
        return false;
    }
    if (paramAddress === "") {
        toastr.error("Bạn chưa thêm address ");
        return false;
    }
    if (paramPhoneNumber === "") {
        toastr.error("Bạn chưa thêm phoneNumber ");
        return false;
    }
    if (paramCity === "") {
        toastr.error("Bạn chưa thêm city ");
        return false;
    }
    if (paramCountry === "") {
        toastr.error("Bạn chưa thêm country ");
        return false;
    }
    return true;
}
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


