// Khai báo các biến cục bộ
var gProductCount = {
    productLine: "",
    productCount: 0
}
var gProduct = {
    productId: 0,
    productName: "",
    productImage: "",
    price: 0

}

function getAllSlide() {
    $.ajax({
        url: "http://localhost:8080/api/auth/slide/all",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadAllSlideHandle(responseObject);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getAllSlide();
function loadAllSlideHandle(paramSlide) {
    for (var bI = 0; bI < paramSlide.length; bI++) {
            if(bI == 0){
                var vSlide = `
                    <div class="carousel-item active">
                        <img src="img/${paramSlide[0].slideUrl}" alt="" class="img-fluid">
                    </div>
                `
            }else{
                var vSlide = `
                    <div class="carousel-item">
                        <img src="img/${paramSlide[bI].slideUrl}" alt="" class="img-fluid">
                    </div>
                `
            }
            
            $(".carousel-inner").append(vSlide); 
    }
}

//Hiển thị sản phẩm mới
function getAllNewProduct() {
    $.ajax({
        url: "http://localhost:8080/api/auth/product",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProduct = responseObject;
            loadAllProductHandle(gProduct);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getAllNewProduct();
//Load sản phẩm mới ra fontend
function loadAllProductHandle(paramProduct) {
    var vProductList = $("#products-1 .row");

    for (var bI = 0; bI < paramProduct.length; bI++) {
        var vItemproduct = '<div class="col-md-3 col-sm-6 product text-center">';
        vItemproduct += '<a href="./productdetail.html?id=' + paramProduct[bI].id + '">';
        vItemproduct += '<div class="anh"><img src="./img/' + paramProduct[bI].productImages[0].imageUrl + '" alt="" class="img-fluid"></div>';
        vItemproduct += '<h3 id="title">' + paramProduct[bI].productName + '</h3>';
        vItemproduct += '</a><p id="price">' + paramProduct[bI].price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}) + '</p>';
        vItemproduct += '</div>';
        vProductList.append(vItemproduct);
    }
}

//Hiển thị sản phẩm giày nam
function getManShoeProduct() {
    $.ajax({
        url: "http://localhost:8080/api/auth/product/manshoes",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProduct = responseObject;
            loadManShoesProductHandle(gProduct);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getManShoeProduct()
//Load sản phẩm giày nam
function loadManShoesProductHandle(paramProduct) {
    var vProductList = $("#products-2 .row");
    for (var bI = 0; bI < paramProduct.length; bI++) {
        var vItemproduct = '<div class="col-md-3 col-sm-6 product text-center">';
        vItemproduct += '<a href="./productdetail.html?id=' + paramProduct[bI].id + '">';
        vItemproduct += '<div class="anh"><img src="./img/' + paramProduct[bI].productImages[0].imageUrl + '" alt="" class="img-fluid"></div>';
        vItemproduct += '<h3 id="title">' + paramProduct[bI].productName + '</h3>';
        vItemproduct += '</a><p id="price">' + paramProduct[bI].price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}) + '</p>';
        vItemproduct += '</div>';
        vProductList.append(vItemproduct);
    }
}

//Hiển thị sản phẩm giày nữ
function getWomanShoeProduct() {
    $.ajax({
        url: "http://localhost:8080/api/auth/product/womanshoes",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject);
            gProduct = responseObject;
            loadWomanShoesProductHandle(gProduct);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}
getWomanShoeProduct()
//Load sản phẩm giày nam
function loadWomanShoesProductHandle(paramProduct) {
    var vProductList = $("#products-3 .row");
    console.log(paramProduct[0].productImage);
    for (var bI = 0; bI < paramProduct.length; bI++) {
        var vItemproduct = '<div class="col-md-3 col-sm-6 product text-center">';
        vItemproduct += '<a href="./productdetail.html?id=' + paramProduct[bI].id + '">';
        vItemproduct += '<div class="anh"><img src="./img/' + paramProduct[bI].productImages[0].imageUrl + '" alt="" class="img-fluid"></div>';
        vItemproduct += '<h3 id="title">' + paramProduct[bI].productName + '</h3>';
        vItemproduct += '</a><p id="price">' + paramProduct[bI].price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'}) + '</p>';
        vItemproduct += '</div>';
        vProductList.append(vItemproduct);
    }
}