var gListItemCart = new Object();
function getDataToLocalStorange(){
    var vJsonListCart = localStorage.getItem("listItemCart");
    gListItemCart = JSON.parse(vJsonListCart);
    console.log(gListItemCart)
    if(gListItemCart == null || gListItemCart.length === 0 || gListItemCart == ""){
        $(".mini-cart").hide();
        $(".number").hide();
    }else{
        
        getDataProductTable(gListItemCart)
    }
}
getDataToLocalStorange()
function getDataProductTable(paramListCart){
    for(var bI = 0 ; bI < paramListCart.length ; bI++){
        var vSub_Total = parseInt(paramListCart[bI].product_Price) * parseInt(paramListCart[bI].quantity) ;
        var vItem = `
        <li class="row">
            <div class="anh col-md-4"><img src="./img/${paramListCart[bI].product_Image}" class="img-fluid" alt=""></div>
            <div class="info-product col-md-6" style="font-size:13px">
                <div class="title"><b >${paramListCart[bI].product_Name}</b></div>
                <div class="qty-product"><span>Qty:</span> ${paramListCart[bI].quantity}</div>
                <div class="gia"><span>Giá</span> ${vSub_Total.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})} vnd</div>
            </div>
            <div id="removeItem" class="col-md-2" data-id="${paramListCart[bI].productId}"><i class="fas fa-times-circle"></i></div>
        </li>
        `
        $(".shopping-cart ul").append(vItem);

        var vCount = $(".number").text(paramListCart.length);
    }
}
$(".shopping-cart").on("click", "#removeItem" , function(){
    deleteItemToCart(this)
});
function deleteItemToCart(paramBtn){

    var vDataButon = $(paramBtn).data();
    var vId = vDataButon.id;
    console.log(vId);
    for(var bI = 0 ; bI < gListItemCart.length ; bI++){
        if(gListItemCart[bI].productId == vId){
            gListItemCart.splice(bI , 1);
            $(paramBtn).closest("tr").remove();            
        }
    } 
    localStorage.setItem("listItemCart" , JSON.stringify(gListItemCart));
    location.reload() 
}
$("#cart-url").on("click" , function(){
    var shoppingCartURL = "cart.html";
    urlSiteToOpen = shoppingCartURL;
    window.location.href = "./" + urlSiteToOpen;
})