var gToken = getCookie("token");

/** Kiểm tra token có tồn tại */
if (gToken == "") {
    $("#info-us a").attr("href" , "./signin.html");
}else{
    $("#info-us a").attr("href" , "#");
    $("#info-us a").attr("title" , "user info");
    $("#info-us").on("click" , getInfoUser);
}

function getInfoUser(){
    var vUserItem =`
        <div class="user-info">
            <div class="user-header row">
                <div class="col-sm-4 img">
                    <img src="./img/f1.png" alt="" class="img-fluid">
                </div>
                <div class="col-sm-8">
                    <h5>tiennm</h5>
                    <p>tiennm@gmail.com</p>
                </div>
            </div>
            <div class="user-content">
                <div class="info-u"><a href="./user-info.html">Hồ sơ cá nhân</a></div>
                <div class="list-o"><a href="./user-order.html">Danh sách đơn hàng</a></div>
                <div id="logout"><a href="#">Thoát</a></div>
            </div>
        </div>
    `;
    $("#mini-u").append(vUserItem).toggleClass("show");
}

//Set sự kiện cho nút logout
$("#mini-u").on("click", "#logout" , function () {
    redirectToLogin();
});

function redirectToLogin() {
    // Trước khi logout cần xóa token đã lưu trong cookie
    setCookie("token", "", 1);
    window.location.href = "./signin.html";
}