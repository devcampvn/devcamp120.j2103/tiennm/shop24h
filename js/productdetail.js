/**Khai báo biến cục bộ */
var gProduct = {};
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gId = gNewUrl.searchParams.get("id");
var gProductLineId = 0;
var quantityValue = 1;
var quantityOrder = 0;
var gKeyLocalStorangeItemCart = "listItemCart";
var gListImage = new Array;
//Lấy Api data product qua id
function getProductByProductId(){
    $.ajax({
        url: "http://localhost:8080/api/auth/products/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            // console.log(responseObject);
            gProduct = responseObject;
            gProductLineId = responseObject.productLineId;
            getListImagesProduct(responseObject);
            console.log(gProduct.productImages[0].imageUrl);
            loadAllProductHandle(gProduct);
            getRelateProduct(gProductLineId);
        },
        error: function (xhr) {
            alert(xhr.status);
        }
    })
}
getProductByProductId();

// Load hiển thị dữ liệu ra fontend
function loadAllProductHandle(paramProduct){
   var vTopProduct = `
        <div class="row">
            <div class="col-md-12 breakcumd text-center">
                <h1 class="pageTitle">${paramProduct.productName}</h1>
                <a href="./index.html">Trang chủ</a> /  <span class="productTitle">${paramProduct.productName}</span>                        
            </div>
        </div>
   `
   $("#page-title").append(vTopProduct);

   var vProductDetail = `
        <h2 class="productName">${paramProduct.productName}</h2>
        <p style="font-size: 25px; color: red;" class="productPrice">${paramProduct.price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
        <table class="option">
            <tbody>
                <tr>
                    <td>Mã sản phẩm</td>
                    <td class="productCode">${paramProduct.productCode}</td>
                </tr>
                <tr>
                    <td>Thương hiệu</td>
                    <td class="productBrand">${paramProduct.productBrandName}</td>
                </tr>
                <tr>
                    <td>Loại giày</td>
                    <td class="productLine">${paramProduct.productLineName}</td>
                </tr>
                <tr>
                    <td>Màu sắc</td>
                    <td class="productColor">${paramProduct.productColorName}</td>
                </tr>
            </tbody>
        </table>
        
   `
   $(".info-pr").append(vProductDetail);

   var vProductDesc = `
        <p>${paramProduct.productDescription}</p>
   `
   $("#tab1").append(vProductDesc);

}

function getListImagesProduct(paramProduct){
    var vSlideImage = `
            <img src="./img/${paramProduct.productImages[0].imageUrl}" class="product-image img-fluid" alt="Product Image">
        `
    $(".product-image").append(vSlideImage);
    for(var bI = 0 ; bI < paramProduct.productImages.length ; bI++){
        var vImageThumb = `
            <div class="product-image-thumb">
                <img src="./img/${paramProduct.productImages[bI].imageUrl}" class=" img-fluid" alt="Product Image">
            </div>
        `
        $(".product-image-thumbs").append(vImageThumb);
    }
    $(".product-image-thumbs .product-image-thumb").first().addClass('active');
}
$('.product-image-thumbs').on('click','.product-image-thumb', function () {
    var $image_element = $(this).find('img')
    $('.product-image').prop('src', $image_element.attr('src'))
    $('.product-image-thumb.active').removeClass('active')
    $(this).addClass('active')
  })

/** Lấy dữ liệu Api trả sản phẩm liên quan */
function getRelateProduct(paramProductLineId){
    $.ajax({
        url: "http://localhost:8080/api/auth/relateProducts?productLineId="+paramProductLineId+"&offsetId="+ gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log("relate: " , responseObject);
            loadRelateProductHandle(responseObject);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Hàm load dữ liệu sản phẩm liên quan
function loadRelateProductHandle(paramProduct){
    var vRelateProduct = $("#relate-product");
    for(var bI = 0 ; bI < paramProduct.length ; bI++){
        var vItemProduct = `
        <div class="col-md-3 col-sm-6 product text-center">
            <a href="./productdetail.html?id=${paramProduct[bI].id}">
            <div class="anh">
                <img src="./img/${paramProduct[bI].productImages[0].imageUrl}" alt="" class="img-fluid">
            </div>
                <h3 id="title">${paramProduct[bI].productName}</h3>               
            </a>
            <p id="price">${paramProduct[bI].price.toLocaleString('it-IT', {style : 'currency', currency : 'VND'})}</p>
        </div>
        `
        vRelateProduct.append(vItemProduct);
    }
}

// add more product
$(".plus").on("click", addingProduct)
function addingProduct() {
    // plus product
    quantityValue += 1;
    $(".number-product").html(quantityValue);
    console.log(quantityValue);
    // edit style minus
    $(".minus").css("pointer-events", "all");
    $("#custom").css("background-color", "rgb(40, 138, 214)");
}
// minor product
$(".minus").on("click", minorProduct)
function minorProduct() {
    console.log(quantityValue);
    quantityValue -= 1;
    $(".number-product").html(quantityValue);
    if (quantityValue == 1) {
        $(".minus").css("pointer-events", "none");
        $("#custom").css("background-color", "rgb(204, 204, 204)");
    } else if (quantityValue > 1) {
        $("#custom").css("background-color", "rgb(40, 138, 214)")
    }
}
// function add to cart
$("#add-to-cart-btn").on("click", addToCartOnClick);
function addToCartOnClick() {
    // quantityOrder += 1;
    console.log(quantityValue);
    var listItemCart = getAllListItemCart();
    var isExistItem = false;
    for(var bI = 0 ; bI < listItemCart.length ; bI++){
        var ItemCartCurrent = listItemCart[bI];
        if(ItemCartCurrent.productId == gId){
            listItemCart[bI].quantity ++ ;
            isExistItem = true;
        }
    }
    if(isExistItem == false){
        var itemCart = createListItemCart(gId , quantityValue , gProduct.productImages[0].imageUrl , gProduct.productName, gProduct.price);
        listItemCart.push(itemCart) ;   
    }
    saveListItemCartToLocalStorange(listItemCart);

    var shoppingCartURL = "cart.html";
    urlSiteToOpen = shoppingCartURL;
    window.location.href = "./" + urlSiteToOpen;

}

//Hàm khởi tạo 1 Object item giỏ hàng
function createListItemCart(paramId , paramQuantity , paramProductImage , paramProductName , paramProductPrice){
    var listItem = new Object();
    listItem.productId = paramId;
    listItem.product_Name = paramProductName;
    listItem.quantity = paramQuantity;
    listItem.product_Price = paramProductPrice;
    listItem.product_Image = paramProductImage;

    return listItem;
}

//Lấy toàn bộ danh sách item giỏ hàng
function getAllListItemCart(){
    var listItemCart = new Array;
    var vJsonListItemCart = localStorage.getItem(gKeyLocalStorangeItemCart);
    if(vJsonListItemCart != null){
        listItemCart = JSON.parse(vJsonListItemCart);
    }
    return listItemCart;
}
//Hàm luu trữ danh sách item giỏ hàng vào localStorange
function saveListItemCartToLocalStorange(paramListCart){

    // Bước 1: chuyển thành chuỗi Json
    var vJsonListItemCart = JSON.stringify(paramListCart);

    // Bước 2: chuyển vào localStorage
    localStorage.setItem(gKeyLocalStorangeItemCart , vJsonListItemCart);
}

