"user strict"
var gUrlString = window.location.href;
var gNewUrl = new URL(gUrlString);
var gIdProduct = gNewUrl.searchParams.get("id");
var gIdUser = 0;
var ReplyObj = [];
console.log("review id product : " + gId);

var gAverage_rating = 0;
var gTotal_review = 0;
var gFive_star_review = 0;
var gFour_star_review = 0;
var gThree_star_review = 0;
var gTwo_star_review = 0;
var gOne_star_review = 0;
var gTotal_user_rating = 0;

/** Hàm đếm 5 sao */
function countFiveStarReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/5/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gFive_star_review = responseObject;
            console.log(gFive_star_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countFiveStarReview();

/** Hàm đếm 4 sao */
function countFourStarReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/4/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gFour_star_review = responseObject;
            console.log(gFour_star_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countFourStarReview();

/** Hàm đếm 3 sao */
function countThreeStarReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/3/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gThree_star_review = responseObject;
            console.log(gThree_star_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countThreeStarReview();

/** Hàm đếm 2 sao */
function countTwoStarReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/2/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gTwo_star_review = responseObject;
            console.log(gTwo_star_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countTwoStarReview();

/** Hàm đếm 1 sao */
function countOneStarReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/1/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gOne_star_review = responseObject;
            console.log(gOne_star_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countOneStarReview();

/** Hàm đếm 1 sao */
function countReview() {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/count/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {

            gTotal_review = responseObject;
            console.log("total: " + gTotal_review);
            load_rating_data()
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}
countReview();

/** Hàm load dữ liệu ra fontend */


function load_rating_data() {

    $('#total_five_star_review').text(gFive_star_review);

    $('#total_four_star_review').text(gFour_star_review);

    $('#total_three_star_review').text(gThree_star_review);

    $('#total_two_star_review').text(gTwo_star_review);

    $('#total_one_star_review').text(gOne_star_review);

    $('#five_star_progress').css('width', (gFive_star_review / gTotal_review) * 100 + '%');

    $('#four_star_progress').css('width', (gFour_star_review / gTotal_review) * 100 + '%');

    $('#three_star_progress').css('width', (gThree_star_review / gTotal_review) * 100 + '%');

    $('#two_star_progress').css('width', (gTwo_star_review / gTotal_review) * 100 + '%');

    $('#one_star_progress').css('width', (gOne_star_review / gTotal_review) * 100 + '%');

    $('#total_review').text(gTotal_review);
    gAverage_rating = averageRatingCalculator();
    $('#average_rating').text(gAverage_rating);
    var count_star = 0;
    $('.main_star').each(function () {
        count_star++;
        if (Math.ceil(gAverage_rating) >= count_star) {
            $(this).addClass('text-warning');
            $(this).addClass('star-light');
        }
    });
}

function averageRatingCalculator() {
    var vAverage_rating = 0;
    if (gFive_star_review === 0 && gFour_star_review === 0 && gThree_star_review === 0 && gTwo_star_review === 0 && gOne_star_review === 0) {
        vAverage_rating = 0;
    } else {
        var vTotal_user_rating = gFive_star_review * 5 + gFour_star_review * 4 + gThree_star_review * 3 + gTwo_star_review * 2 + gOne_star_review * 1;
        vAverage_rating = (vTotal_user_rating / gTotal_review).toFixed(1);

    }

    return vAverage_rating;
}

/** Hàm get data review */
getReviewContent();

function getReviewContent() {
    $.ajax({
        url: "http://localhost:8080/api/auth/reviews/" + gIdProduct,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            if (responseObject.length == 0) {
                $("#review_content").hide();
            } else {
                $("#review_content").show();
                console.log("obj", responseObject);
                loadReviewDataHandle(responseObject);
            }
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }
    });
}

/** Hàm load data Review ra font end */
function loadReviewDataHandle(paramReview) {
    var vHtml = '';
    for (var bI = 0; bI < paramReview.length; bI++) {
        ReplyObj.push(paramReview[bI].reviewId);

        console.log("array", ReplyObj);

        vHtml += '<div class="row mt-3" id="user-review" data-review = "' + paramReview[bI].reviewId + '" >';

        vHtml += '<div class="col-sm-1"><div class="rounded bg-danger text-white pt-2 pb-2"><h3 class="text-center">' + paramReview[bI].userName.charAt(0).toUpperCase() + '</h3></div></div>';

        vHtml += '<div class="col-sm-11">';

        vHtml += '<div class="card">';

        vHtml += '<div class="card-header"><b>' + paramReview[bI].userName + '</b></div>';

        vHtml += '<div class="card-body">';

        for (var star = 1; star <= 5; star++) {
            var class_name = '';

            if (paramReview[bI].rating >= star) {
                class_name = 'text-warning';
            }
            else {
                class_name = 'star-light';
            }

            vHtml += '<i class="fas fa-star ' + class_name + ' mr-1"></i>';
        }

        vHtml += '<br />';

        vHtml += paramReview[bI].review;

        vHtml += '</div>';

        vHtml += '<div class="card-footer text-right">On ' + paramReview[bI].createDate.split("/").reverse().join("-") + '</div>';

        vHtml += '</div>';

        vHtml += '</div>';

        if (paramReview[bI].reply == null && paramReview[bI].replyDate == null) {
            vHtml += '<div class="col-sm-11 offset-sm-1 mt-3" id="user-reply" > </div>';
        } else {
            vHtml += '<div class="col-sm-11 offset-sm-1 mt-3" id="user-reply" >';

            vHtml += '<div class="row" >';

            vHtml += '<div class="col-sm-1"><div class="rounded bg-success text-white pt-2 pb-2"><h3 class="text-center"> A </h3></div></div>';

            vHtml += '<div class="col-sm-11">';

            vHtml += '<div class="card">';

            vHtml += '<div class="card-header"><b>Admin</b></div>';

            vHtml += '<div class="card-body">';

            vHtml += paramReview[bI].reply;

            vHtml += '</div>';

            vHtml += '<div class="card-footer text-right">On ' + paramReview[bI].replyDate.split("/").reverse().join("-") + '</div>';

            vHtml += '</div>';

            vHtml += '</div>';

            vHtml += '</div>';

            vHtml += '</div>';
        }

        vHtml += '</div>';
        vHtml += '</div>';

    }
    $('#review_content').append(vHtml);
    
}

