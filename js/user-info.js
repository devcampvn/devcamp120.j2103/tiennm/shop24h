var gToken = getCookie("token");
var gEmail = "";
var gCustomerObject = new Object();
var gCustomerDb = new Object();
if (gToken == "") {
    window.location.href = "./admin/signin.html";
}else{
    checkDataUser()
}

//Kiểm tra dữ liệu người dùng
function checkDataUser(){
    $.ajax({
        url: "http://localhost:8080/api/auth/user/me",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (pRes) {
            gEmail = pRes.email;
            getCustomerInfo(gEmail)
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            window.location.href = "./admin/signin.html";

        }
    });
}

//Hiển thị thông tin khách hàng qua email
function getCustomerInfo(paramEmail){
    $.ajax({
        url: "http://localhost:8080/api/auth/customer?email=" + paramEmail,
        type: "GET",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (responseObject) {
            gCustomerDb = responseObject;
            console.log(responseObject)
            loadCustomerDataHandle(responseObject);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

//Load dữ liệu ra bảng
function loadCustomerDataHandle(paramObj){
    $("#personal-information").children().remove();
    vUserInfo = `
        <table class="table table-user-information">
            <tbody>
                <tr>
                    <td>First Name </td>
                    <td>${paramObj.firstName}</td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td>${paramObj.lastName}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>${paramObj.address}</td>
                </tr>

                <tr>
                <tr>
                    <td>Phone Number</td>
                    <td>${paramObj.phoneNumber}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>${paramObj.email}</td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>${paramObj.city}</td>
                </tr>
                <tr>
                    <td>Country</td>
                    <td>${paramObj.country}</td>
                </tr>
            </tbody>
        </table>
        <a  class="btn btn-success w-45" id="return_index" href="./index.html">Tiếp tục mua sắm</a>
        <button type="button" class="btn btn-danger text-white" id="edit_info" >Thay đổi thông tin</button>
    `
    $("#personal-information").append(vUserInfo);
}

/** Hàm cập nhật dữ liệU khách hàng */
$("#personal-information").on("click" , "#edit_info" , getCustomerData);

function getCustomerData(){
    $("#modal-info").modal("show");
    loadDataCustomerToForm();
    console.log("ok")
}

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});

/** Hàm load dữ liệu vào form */
function loadDataCustomerToForm(){
    $("#firstName-modal").val(gCustomerDb.firstName);
    $("#lastName-modal").val(gCustomerDb.lastName);
    $("#address-modal").val(gCustomerDb.address);
    $("#phoneNumber-modal").val(gCustomerDb.phoneNumber);
    $("#postalCode-modal").val(gCustomerDb.postalCode);
    $("#email-modal").val(gCustomerDb.email);
    $("#city-modal").val(gCustomerDb.city);
    $("#country-modal").val(gCustomerDb.country);
}

$("#send-data").on("click", saveCustomerData);
//Hàm save dữ liệu khi sửa
function saveCustomerData() {
    gCustomerObject = getCustomer();

    if (validateData()) {
        //B3: thêm , sửa dữ liệu
        saveCustomer(gCustomerObject);
        
        //B4:đóng modal
        $("#modal-info").modal("hide");
        toastr.success('Thêm thành công');

    }
}

//Hàm cập nhật
function saveCustomer(paramCustomer) {
    var vEmail = paramCustomer.email;
    $.ajax({
        url: "http://localhost:8080/api/auth/customers/update/" + vEmail,
        type: "PUT",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramCustomer),
        dataType: "json",
        success: function (paramRes) {
           console.log(paramRes)
           gCustomerDb = paramRes
           loadCustomerDataHandle(gCustomerDb);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(thrownError);
        }
    })
}

//Lấy dữ liệu sửa
function getCustomer() {
    var vCustomerObject = {
        firstName: "",
        lastName: "",
        address: "",
        phoneNumber: "",
        postalCode: "",
        email: "",
        city: "",
        country: "",
    };
    vCustomerObject.firstName = $("#firstName-modal").val().trim();
    vCustomerObject.lastName = $("#lastName-modal").val().trim();
    vCustomerObject.address = $("#address-modal").val().trim();
    vCustomerObject.phoneNumber = $("#phoneNumber-modal").val().trim();
    vCustomerObject.postalCode = $("#postalCode-modal").val().trim();
    vCustomerObject.email = $("#email-modal").val().trim();
    vCustomerObject.city = $("#city-modal").val().trim();
    vCustomerObject.country = $("#country-modal").val().trim();
    return vCustomerObject;
}

//Validate dữ liệu
function validateData() {
    if (gCustomerObject.firstName === "") {
        alert("Bạn chưa thêm firstName ");
        return false;
    }
    if (gCustomerObject.lastName === "") {
        alert("Bạn chưa thêm lastName ");
        return false;
    }
    if (gCustomerObject.address === "") {
        alert("Bạn chưa thêm address ");
        return false;
    }
    if (gCustomerObject.phoneNumber === "") {
        alert("Bạn chưa thêm phoneNumber ");
        return false;
    }
    if (gCustomerObject.postalCode === "") {
        alert("Bạn chưa thêm postalCode ");
        return false;
    }
    if (gCustomerObject.email === "") {
        alert("Bạn chưa thêm email ");
        return false;
    }
    if (gCustomerObject.city === "") {
        alert("Bạn chưa thêm city ");
        return false;
    }
    if (gCustomerObject.country === "") {
        alert("Bạn chưa thêm country ");
        return false;
    }
    return true;
}

//Reset form
function resetForm() {
    $("#firstName-modal").val("");
    $("#lastName-modal").val("");
    $("#address-modal").val("");
    $("#phoneNumber-modal").val("");
    $("#postalCode-modal").val("");
    $("#email-modal").val("");
    $("#city-modal").val("");
    $("#country-modal").val("");
}