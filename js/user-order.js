var gToken = getCookie("token");
var gEmail = "";
var gProductId = 0;
var gUserId = 0;
var gRating_data = 0;
var gOrderInfo = new Object();
var gReviewData = new Object();
if (gToken == "") {
    window.location.href = "../signin.html";
}
else {
    checkDataUser()
}

//Kiểm tra dữ liệu người dùng
function checkDataUser() {
    $.ajax({
        url: "http://localhost:8080/api/auth/user/me",
        type: 'GET',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (pRes) {
            gEmail = pRes.email;
            gUserId = pRes.id;
            getCustomerInfo(gEmail)
            getOrderInfo(gEmail);
        },
        error: function (pAjaxContext) {
            console.log(pAjaxContext.responseText);
            window.location.href = "../signin.html";

        }
    });
}

//Hiển thị thông tin khách hàng qua email
function getCustomerInfo(paramEmail) {
    $.ajax({
        url: "http://localhost:8080/api/auth/customer?email=" + paramEmail,
        type: "GET",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (responseObject) {
            loadCustomerInfoHandle(responseObject);
        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

//Load dữ liệu ra bảng
function loadCustomerInfoHandle(paramObj) {
    var vUserInfo = `
        <table class="table table-user-information">
            <tbody>
                <tr>
                    <td>First Name </td>
                    <td>${paramObj.firstName}</td>
                </tr>
                <tr>
                    <td>Last Name</td>
                    <td>${paramObj.lastName}</td>
                </tr>
                <tr>
                    <td>Address</td>
                    <td>${paramObj.address}</td>
                </tr>

                <tr>
                <tr>
                    <td>Phone Number</td>
                    <td>${paramObj.phoneNumber}</td>
                </tr>
                <tr>
                    <td>Email</td>
                    <td>${paramObj.email}</td>
                </tr>
            </tbody>
        </table>
    `
    $("#personal-information").append(vUserInfo);
}

var gStt = 1;
function getSoThuTu() {
    return gStt++;
}

function getOrderDate(data) {
    
    var vOrderDate = "";
    for (var bI = 0; bI < gOrderInfo.length; bI++) {
        vOrderDate = gOrderInfo[bI].orderDate.split("-").reverse().join("-");
    }
    return vOrderDate;
}
/** Hiển thị nội dung ra bảng **/
var gDataTable = $("#table-data").DataTable({
    "columns": [
        { "data": "id" },
        { "data": "productName" },
        { "data": "productCode" },
        { "data": "orderDate" },
        { "data": "quanlity" },
        { "data": "price" },
        { "data": "review" },
    ],
    columnDefs: [
        {
            "targets": 0,
            render: function (data, type, full, meta) {
                return getSoThuTu();
            },
            "width": "5%"
        },

        {
            "targets": 3,
            render: function (data, type, full, meta) {
                
                return getOrderDate(data);
            },
            "width": "5%"
        },

        {
            "targets": 5,
            render: function (data, type, full, meta) {                
                return data.toLocaleString('it-IT', {style : 'currency', currency : 'VND'});
            },
            "width": "5%"
        },

        {
            "targets": 6,
            "defaultContent": "<button class='btn btn-success btn-sm edit-bt mr-1' title='edit button' id = 'add_review'><i class ='fas  fa-edit '></i> Đánh giá sản phẩm</button>",
            "width": "22%"

        }
    ]
})

//Hàm lấy thông tin order
function getOrderInfo(paramEmail) {
    $.ajax({
        url: "http://localhost:8080/api/auth/user/order?email=" + paramEmail,
        type: "GET",
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        success: function (responseObject) {
            gOrderInfo = responseObject
            loadUserOrderHandle(gOrderInfo);

        },
        error: function (error) {
            console.log(error.responseText);
        }
    });
}

//Load dữ liêu ra bảng
function loadUserOrderHandle(paramObj) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDataTable.clear();
    gStt = 1;
    //Cập nhật data cho bảng 
    gDataTable.rows.add(paramObj);

    //Cập nhật lại giao diện hiển thị bảng
    gDataTable.draw();
}
$("#table-data").on("click", "#add_review", function () {
    loadFormReview(this);
})

/** Hàm hiển thị form review */
function loadFormReview(paramProductId) {
    var vRowIndex = $(paramProductId).parents("tr");
    var vCurrentProduct = gDataTable.row(vRowIndex).data();
    gProductId = vCurrentProduct.productId;
    $("#review_modal").modal("show");

}

$(document).on('mouseenter', '.submit_star', function () {

    var rating = $(this).data('rating');

    reset_background();

    for (var count = 1; count <= rating; count++) {

        $('#submit_star_' + count).addClass('text-warning');

    }

});

function reset_background() {
    for (var count = 1; count <= 5; count++) {

        $('#submit_star_' + count).addClass('star-light');

        $('#submit_star_' + count).removeClass('text-warning');

    }
}
$(document).on('click', '.submit_star', function () {

    gRating_data = $(this).data('rating');
    console.log(gRating_data);

});

$('#save_review').click(function () {
    insertReviewData()
});

//Xử lý sự kiện đóng
$(".modal").on("hidden.bs.modal", function () {
    resetForm();
});


/**Hàm thêm mới review  */
function insertReviewData() {

    //Get data review
    gReviewData = getDataReview();

    //ValidateData
    if (validateData()) {

        //Save Review
        saveReview(gReviewData, gProductId, gUserId);


    }
}

/** Hàm save review */
function saveReview(paramData, paramProductId, paramUserId) {
    $.ajax({
        url: "http://localhost:8080/api/auth/review/create/" + paramProductId + "/" + paramUserId,
        type: "POST",
        headers: {
            "Authorization": "Bearer " + gToken
        },
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramData),
        dataType: "json",
        success: function (paramRes) {
            //Đóng modal
            $("#review_modal").modal("hide");
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Bạn không có quyền thêm mới");

        }
    })
}

/** Hàm lấy dữ liệu từ form */
function getDataReview() {
    var vReview = new Object();
    vReview.rating = gRating_data;
    vReview.review = $("#user_review").val();
    return vReview;
}

/** Hàm validate dữ liệu */
function validateData() {
    if (gReviewData.rating == 0) {
        toastr.error("Bạn chưa đánh giá ");
        return false;
    }
    return true;
}

/** Hàm reset form */
function resetForm() {
    $("#user_review").val("");
    gRating_data = 0;
}
